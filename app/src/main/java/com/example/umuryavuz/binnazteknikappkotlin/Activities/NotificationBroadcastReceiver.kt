package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class NotificationBroadcastReceiver : BroadcastReceiver() {
    private var builder: NotificationCompat.Builder? = null
    private var notification: Notification? = null


    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.getStringExtra("action")
        if (action == "Stop") {
            performAction1(context)
        } else if (action == "action2") {
            performAction2()
        }
        //This is used to close the notification tray
        val it = Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)
        context.sendBroadcast(it)
    }

    fun performAction1(mContext: Context) {
        StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                if (dataSnapshot.child("roles").child("administrator").getValue(Boolean::class.java)!!) {
                    if (AdminAccountActivity.timeElapsed == null) {
                        StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                StaticDataSingleton.stopAction()
                                val notificationManager = arrayOf(mContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
                                notificationManager[0].cancelAll()


                                val l_count = dataSnapshot.child("daily_login_counter").getValue(Int::class.java)!!
                                val timeElapsed = dataSnapshot.child("Logs").child((l_count - 1).toString() + "").child("elapsed").getValue(Int::class.java)!!
                                val tm_elps = Math.floor((timeElapsed / 3600).toDouble()).toInt().toString() + " hours " + Math.floor((timeElapsed / 60).toDouble()).toInt() + " min " + timeElapsed % 60 + " sec "

                                builder = NotificationCompat.Builder(mContext)
                                notificationManager[0] = mContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager


                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    val importance = NotificationManager.IMPORTANCE_HIGH
                                    var mChannel: NotificationChannel? = notificationManager[0].getNotificationChannel("Binnaz Teknik App")
                                    if (mChannel == null) {
                                        mChannel = NotificationChannel("Binnaz Teknik App", "Binnaz Teknik App", importance)
                                        mChannel.description = "Record Stoped  Time elapsed : $tm_elps"
                                        mChannel.enableVibration(true)
                                        mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                                        notificationManager[0].createNotificationChannel(mChannel)
                                    }


                                    val notificationIntent: Intent

                                    if (dataSnapshot.child("roles").child("administrator").getValue(Boolean::class.java)!!) {
                                        notificationIntent = Intent(mContext, AdminAccountActivity::class.java)
                                    } else {
                                        notificationIntent = Intent(mContext, UserAccountActivity::class.java)
                                    }

                                    notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

                                    val intent_home = PendingIntent.getActivity(mContext, 0,
                                            notificationIntent, 0)

                                    builder = NotificationCompat.Builder(mContext, "Binnaz Teknik App")

                                    builder!!.setContentTitle("Record Stoped")
                                            .setContentText("Time elapsed : $tm_elps")
                                            .setContentIntent(null)
                                            .setSmallIcon(R.mipmap.ic_date_range_black_24dp)

                                    builder!!.setContentIntent(intent_home)


                                    notificationManager[0] = mContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager


                                } else {
                                    builder = NotificationCompat.Builder(mContext)

                                    val notificationIntent: Intent

                                    if (dataSnapshot.child("roles").child("administrator").getValue(Boolean::class.java)!!) {
                                        notificationIntent = Intent(mContext, AdminAccountActivity::class.java)
                                    } else {
                                        notificationIntent = Intent(mContext, UserAccountActivity::class.java)
                                    }

                                    notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

                                    val intent_home = PendingIntent.getActivity(mContext, 0,
                                            notificationIntent, 0)

                                    builder!!.setContentTitle("Record Stoped")
                                            .setContentText("Time elapsed : $tm_elps")
                                            .setContentIntent(null)
                                            .setSmallIcon(R.mipmap.ic_date_range_black_24dp)

                                    builder!!.setContentIntent(intent_home)
                                }

                                builder!!.setUsesChronometer(false)
                                builder!!.setShowWhen(false)


                                notification = builder!!.build()
                                notificationManager[0].notify(1, notification)

                            }

                            override fun onCancelled(databaseError: DatabaseError) {

                            }
                        })

                    } else {
                        StaticDataSingleton.performRecord(mContext, AdminAccountActivity.onlineButton, AdminAccountActivity.timeElapsed!!, AdminAccountActivity.login_count, AdminAccountActivity.is_online)
                    }
                } else {
                    StaticDataSingleton.performRecord(mContext, UserAccountActivity.onlineButton, UserAccountActivity.timeElapsed, UserAccountActivity.login_count, UserAccountActivity.is_Online)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

    }

    fun performAction2() {

    }
}