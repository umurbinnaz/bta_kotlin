package com.example.umuryavuz.binnazteknikappkotlin.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.Adapter.AdapterDisplayedLogInfo
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.LogInfo
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.TaskInfo
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.ViewHolders.ViewHolderSelectedUserProfileInfo
import com.example.umuryavuz.binnazteknikappkotlin.ViewHolders.ViewHolderTaskInfo
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*
import java.util.concurrent.TimeUnit

class FragmentDisplayedLogs : Fragment() {

    private var onlineLogRecyclerView: RecyclerView? = null
    private var year: TextView? = null
    private var month: TextView? = null
    private var day: TextView? = null
    private var day_name: TextView? = null
    private var total: TextView? = null

    private var v1: ValueEventListener? = null
    private var v2: ValueEventListener? = null
    private var v3: ValueEventListener? = null
    private var v4: ValueEventListener? = null
    private var v5: ValueEventListener? = null


    private var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //ONLINE LOG FRAGMENT


        rootView = inflater.inflate(R.layout.fragment_online_log, container, false)

        total = rootView!!.findViewById(R.id.total) as TextView
        year = rootView!!.findViewById(R.id.online_log_admin_year) as TextView
        month = rootView!!.findViewById(R.id.online_log_admin_month) as TextView
        day = rootView!!.findViewById(R.id.online_log_admin_day) as TextView
        day_name = rootView!!.findViewById(R.id.online_log_admin_day_name) as TextView

        onlineLogRecyclerView = rootView!!.findViewById(R.id.online_log_recycler_view) as RecyclerView
        onlineLogRecyclerView!!.setHasFixedSize(true)
        val mLayoutManager = LinearLayoutManager(getActivity())
        mLayoutManager.reverseLayout = true // THIS ALSO SETS setStackFromBottom to true
        mLayoutManager.stackFromEnd = true
        onlineLogRecyclerView!!.layoutManager = mLayoutManager

        v1 = StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId().toString()).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val log_index = dataSnapshot.child("daily_login_counter").getValue(Int::class.java)!!
                if (log_index == 0) {
                    if (dataSnapshot.child("Logs").hasChild(log_index.toString() + "")) {


                        val current_tmps = dataSnapshot.child("CurrentTimestamp").getValue(Long::class.java)!!
                        val k = dataSnapshot.child("CurrentTimestamp").getValue(Long::class.java)!!

                        val tss = TimeUnit.MILLISECONDS.toMicros(StaticDataSingleton.getStartOfDayEpochSecond(k)!!)
                        val tss_next = TimeUnit.MILLISECONDS.toMicros(StaticDataSingleton.getEndOfDayEpochSecond(k)!!)

                        val current_day_info = Date(tss)
                        val cc = Calendar.getInstance()
                        cc.time = current_day_info
                        cc.add(Calendar.MONTH, 1)
                        year!!.text = cc.get(Calendar.YEAR).toString() + ""
                        month!!.text = (if (cc.get(Calendar.MONTH) > 10) cc.get(Calendar.MONTH) else "0" + cc.get(Calendar.MONTH)).toString() + ""
                        day!!.text = (if (cc.get(Calendar.DAY_OF_MONTH) > 10) cc.get(Calendar.DAY_OF_MONTH) else "0" + cc.get(Calendar.DAY_OF_MONTH)).toString() + ""
                        day_name!!.setText(StaticDataSingleton.getDay(cc.get(Calendar.DAY_OF_MONTH).toString() + "", tss))

                        v2 = StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId().toString()).child("LogsTimes").addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                if (dataSnapshot.hasChild(tss.toString() + "")) {
                                    val t_min = dataSnapshot.child(tss.toString() + "").getValue(Int::class.java)!!

                                    val seconds = (TimeUnit.SECONDS.toMillis(t_min.toLong()) / 1000).toInt() % 60
                                    val minutes = (TimeUnit.SECONDS.toMillis(t_min.toLong()) / (1000 * 60) % 60).toInt()
                                    val hours = (TimeUnit.SECONDS.toMillis(t_min.toLong()) / (1000 * 60 * 60) % 24).toInt()


                                    total!!.text = hours.toString() + " hour " + minutes + " minutes"
                                }
                            }

                            override fun onCancelled(databaseError: DatabaseError) {

                            }
                        })
                        v3 = StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId().toString()).child("Logs").addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                val temp = ArrayList<LogInfo>()
                                for (ds_logs in dataSnapshot.children) {
                                    if (ds_logs.hasChild("login") && ds_logs.hasChild("logout")) {
                                        val login = ds_logs.child("login").getValue(Long::class.java)!!
                                        val logout = ds_logs.child("logout").getValue(Long::class.java)!!
                                        if (login >= tss && logout <= tss_next) {
                                            temp.add(ds_logs.getValue<LogInfo>(LogInfo::class.java!!)!!)
                                        }
                                    } else {
                                        val login = ds_logs.child("login").getValue(Long::class.java)!!
                                        if (login >= tss) {
                                            temp.add(ds_logs.getValue<LogInfo>(LogInfo::class.java!!)!!)
                                        }
                                    }
                                }

                                val mAdapter = AdapterDisplayedLogInfo(temp)
                                onlineLogRecyclerView!!.adapter = mAdapter
                            }

                            override fun onCancelled(databaseError: DatabaseError) {

                            }
                        })
                    }
                } else {
                    if (dataSnapshot.child("Logs").hasChild((log_index - 1).toString() + "")) {
                        val current_tmps = dataSnapshot.child("CurrentTimestamp").getValue(Long::class.java)!!
                        val k = dataSnapshot.child("CurrentTimestamp").getValue(Long::class.java)!!

                        val tss = TimeUnit.MILLISECONDS.toMicros(StaticDataSingleton.getStartOfDayEpochSecond(k)!!)
                        val tss_next = TimeUnit.MILLISECONDS.toMicros(StaticDataSingleton.getEndOfDayEpochSecond(k)!!)

                        val current_day_info = Date(current_tmps)
                        val cc = Calendar.getInstance()
                        cc.time = current_day_info
                        cc.add(Calendar.MONTH, 1)
                        year!!.text = cc.get(Calendar.YEAR).toString() + ""
                        month!!.text = (if (cc.get(Calendar.MONTH) > 10) cc.get(Calendar.MONTH) else "0" + cc.get(Calendar.MONTH)).toString() + ""
                        day!!.text = (if (cc.get(Calendar.DAY_OF_MONTH) > 10) cc.get(Calendar.DAY_OF_MONTH) else "0" + cc.get(Calendar.DAY_OF_MONTH)).toString() + ""
                        day_name!!.setText(StaticDataSingleton.getDay(cc.get(Calendar.DAY_OF_MONTH).toString() + "", tss))


                        v4 = StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId().toString()).child("LogsTimes").addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                if (dataSnapshot.hasChild(tss.toString() + "")) {
                                    val t_min = dataSnapshot.child(tss.toString() + "").getValue(Int::class.java)!!
                                    total!!.text = Math.floor((t_min / 3600).toDouble()).toInt().toString() + " hour " + Math.ceil((t_min / 60).toDouble()).toInt() + " minutes"
                                }
                            }

                            override fun onCancelled(databaseError: DatabaseError) {

                            }
                        })

                        v5 = StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId().toString()).child("Logs").addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                val temp = ArrayList<LogInfo>()
                                for (ds_logs in dataSnapshot.children) {
                                    if (ds_logs.hasChild("login") && ds_logs.hasChild("logout")) {
                                        val login = ds_logs.child("login").getValue(Long::class.java)!!
                                        val logout = ds_logs.child("logout").getValue(Long::class.java)!!
                                        if (login >= tss && logout <= tss_next) {
                                            temp.add(ds_logs.getValue<LogInfo>(LogInfo::class.java!!)!!)
                                        }
                                    } else {
                                        val login = ds_logs.child("login").getValue(Long::class.java)!!
                                        if (login >= tss) {
                                            temp.add(ds_logs.getValue<LogInfo>(LogInfo::class.java!!)!!)
                                        }
                                    }
                                }

                                val mAdapter = AdapterDisplayedLogInfo(temp)
                                onlineLogRecyclerView!!.adapter = mAdapter
                            }

                            override fun onCancelled(databaseError: DatabaseError) {

                            }
                        })
                    }
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
        return rootView
    }

}