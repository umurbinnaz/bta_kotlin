package com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects

class DisplayedLogInfo {
    var date: Long? = null
    var lastlogin: Long? = null
    var lastlogout: Long? = null
    var totalworktime: Long? = null


    constructor(date: Long?, lastlogin: Long?, lastlogout: Long?, totalworktime: Long?) {
        this.date = date
        this.lastlogin = lastlogin
        this.lastlogout = lastlogout
        this.totalworktime = totalworktime
    }

    constructor() {}
}