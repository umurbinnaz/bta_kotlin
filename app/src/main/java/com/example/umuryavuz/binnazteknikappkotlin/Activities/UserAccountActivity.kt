package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.NotificationCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.example.umuryavuz.binnazteknikappkotlin.Adapter.AdapterAdminAccountViewPager
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import java.net.URL

class UserAccountActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener  {
    lateinit var mAuth: FirebaseAuth
    lateinit var authStateListener: FirebaseAuth.AuthStateListener
    lateinit var bar: ProgressDialog
    lateinit var firebaseDatabase: FirebaseDatabase
    lateinit var usermail: TextView
    lateinit var online: TextView
    lateinit var profileImage: ImageView
    lateinit var fullname: TextView
    lateinit var page_header: ImageView
    lateinit var mViewPager: ViewPager
    lateinit var mSectionsPagerAdapter: AdapterAdminAccountViewPager
    lateinit var chronometer: Chronometer
    lateinit var v1: ValueEventListener
    lateinit var v4: ValueEventListener
    lateinit var v6: ValueEventListener
    lateinit var v7: ValueEventListener
    var isUser = true
    val total_work_time: String? = null
    var last_login_time: Long = 0
    var last_logout_time: Long = 0




    companion object {
        lateinit var onlineButton: Button
        lateinit var timeElapsed: Chronometer
        var login_count: Int = 0
        var is_Online: Boolean = false

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_account)

        val toolbar = findViewById<View>(R.id.user_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.elevation = 0f

        StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).child("CurrentTimestamp").setValue(ServerValue.TIMESTAMP)

        val drawer = findViewById<View>(R.id.user_drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
        val tabLayout = findViewById<View>(R.id.tabs) as TabLayout


        mSectionsPagerAdapter = AdapterAdminAccountViewPager(supportFragmentManager, tabLayout.tabCount)
        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById<View>(R.id.container) as ViewPager
        mViewPager!!.adapter = mSectionsPagerAdapter
        mViewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(mViewPager))
        firebaseDatabase = FirebaseDatabase.getInstance()
        onlineButton = findViewById<View>(R.id.online_user) as Button
        profileImage = findViewById<View>(R.id.user_profile_image) as ImageView
        mAuth = FirebaseAuth.getInstance()
        bar = ProgressDialog(this)
        page_header = findViewById<View>(R.id.user_account_header_background_image) as ImageView
        usermail = findViewById<View>(R.id.user_usermail) as TextView
        online = findViewById<View>(R.id.user_online_status) as TextView
        fullname = findViewById<View>(R.id.user_account_name_surname) as TextView
        StaticDataSingleton.is_login= false
        timeElapsed = findViewById<View>(R.id.chronomete_user) as Chronometer

        timeElapsed.onChronometerTickListener = Chronometer.OnChronometerTickListener { c ->
            val cTextSize = c.text.length
            if (cTextSize == 5) {
                timeElapsed.text = "00:" + c.text.toString()
            } else if (cTextSize == 7) {
                timeElapsed.text = "0" + c.text.toString()
            }
        }


        onlineButton.setOnClickListener { StaticDataSingleton.performRecord(this@UserAccountActivity, onlineButton, timeElapsed, login_count, is_Online) }

        authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser == null) {
                bar!!.dismiss()
                val toMain = Intent(this@UserAccountActivity, MainActivity::class.java)
                toMain.addCategory(Intent.CATEGORY_HOME)
                toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toMain)
            } else {
                v6 = StaticDataSingleton.DatabaseReference().child("users").addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val profile_image_url = dataSnapshot.child(StaticDataSingleton.getUserId()).child("image").getValue(String::class.java)
                        if (profile_image_url != null) {
                            Picasso.with(this@UserAccountActivity).load(profile_image_url).resize(1000, 800)
                                    .centerCrop().into(profileImage)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {

                    }
                })

            }
        }

        v7 = StaticDataSingleton.DatabaseReference().child("users").child(mAuth!!.currentUser!!.uid).addValueEventListener(object : ValueEventListener {
            @SuppressLint("ResourceAsColor")
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                if (dataSnapshot.hasChild("daily_login_counter")) {
                    login_count = dataSnapshot.child("daily_login_counter").getValue(Int::class.java)!!
                } else {
                    StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).child("daily_login_counter").setValue(0)
                }

                if (dataSnapshot.child("online").getValue(Boolean::class.javaPrimitiveType)!!) {
                    online!!.setText(R.string.online)
                    is_Online = true
                    onlineButton.setBackgroundResource(R.drawable.button_red)
                    onlineButton.setText(R.string.stop_record)
                    onlineButton.setTextColor(R.color.White)

                } else {
                    online!!.setText(R.string.offline)
                    is_Online = false
                    onlineButton.setBackgroundResource(R.drawable.button_green)
                    onlineButton.setText(R.string.start_record)
                    onlineButton.setTextColor(R.color.Black)
                }
                var name: String? = ""
                var surname: String? = ""
                if (dataSnapshot.child("name").getValue(String::class.java) != null || dataSnapshot.child(mAuth!!.currentUser!!.uid).child("secondname").getValue(String::class.java) != null) {
                    name = dataSnapshot.child("name").getValue(String::class.java)
                    surname = dataSnapshot.child("secondname").getValue(String::class.java)
                }
                val fname = "$name $surname"
                fullname!!.text = fname

                if (dataSnapshot.hasChild("lastlogin")) {
                    last_login_time = dataSnapshot.child("lastlogin").getValue(Long::class.java)!!
                }

                if (dataSnapshot.hasChild("lastlogout")) {
                    last_logout_time = dataSnapshot.child("lastlogout").getValue(Long::class.java)!!
                }

                val background_image_url = dataSnapshot.child("background_image").getValue(String::class.java)

                if (background_image_url != null) {
                    Glide.with(applicationContext).load(background_image_url).into(page_header!!)
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })


    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.user_drawer_layout) as DrawerLayout
        moveTaskToBack(true)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
            finish()
        }

    }

    override fun onStart() {
        super.onStart()
        mAuth!!.addAuthStateListener(authStateListener!!)
        authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser == null) {
                bar!!.dismiss()
                isUser = false
                val toMain = Intent(this@UserAccountActivity, MainActivity::class.java)
                //toMain.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                toMain.addCategory(Intent.CATEGORY_HOME)
                toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toMain)
            } else {
                isUser = true
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_logout) {
            val ref = StaticDataSingleton.DatabaseReference().child("users").child(mAuth!!.currentUser!!.uid).child("online")
            ref.setValue(false);
            bar!!.setMessage("Logging out")
            bar!!.show()
            mAuth!!.removeAuthStateListener(authStateListener!!)
            super@UserAccountActivity.finish()
            finish()
            mAuth!!.signOut()
        } else if (id == R.id.nav_edit_profile) {
            val toEdit = Intent(this@UserAccountActivity, EditProfileActivity::class.java)
            //toEdit.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toEdit.addCategory(Intent.CATEGORY_HOME)
            toEdit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toEdit)
        }

        val drawer = findViewById<View>(R.id.user_drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        finish()
    }
}