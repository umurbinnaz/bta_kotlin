package com.example.umuryavuz.binnazteknikappkotlin.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.umuryavuz.binnazteknikappkotlin.Fragments.*

class AdapterUserProfileViewPager(fm: FragmentManager, internal var mNumOfTabs: Int) : FragmentPagerAdapter(fm) {




    override fun getCount(): Int {
      return mNumOfTabs
    }

    override fun getItem(position: Int): Fragment? {

        when (position) {
            0 -> {

                return FragmentUserInfo()
            }

            1 -> {

                return FragmentDateRangeLogs()
            }

            2 -> {

                return FragmentOptions()
            }

            3 -> {

                return FragmentTasks()
            }

            else -> return null
        }

    }
}