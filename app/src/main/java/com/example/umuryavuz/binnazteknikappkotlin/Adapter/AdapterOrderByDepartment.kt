package com.example.umuryavuz.binnazteknikappkotlin.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.UserInfo
import com.example.umuryavuz.binnazteknikappkotlin.Interfaces.InterfaceItemClickListener
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.Activities.UserProfileActivity
import com.example.umuryavuz.binnazteknikappkotlin.ViewHolders.ViewHolderUserList
import java.util.ArrayList

class AdapterOrderByDepartment : RecyclerView.Adapter<ViewHolderUserList> {


     var users: ArrayList<UserInfo>
     var mContext: Context

    constructor(arrayList: ArrayList<UserInfo>, mContext: Context){
        this.mContext = mContext
        this.users = arrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderUserList {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_user_info, parent, false)
        return ViewHolderUserList(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: ViewHolderUserList, position: Int) {
        var Object = users[position]
        holder.setMail(Object.name + " " + Object.secondname)
        holder.setOnline(Object.online)
        holder.setImage(mContext.applicationContext, Object.image)
        holder.setLastLogin(Object.lastlogin)
        holder.setDate(Object.lastlogin)
        holder.setItemClickListenerInterface(object : InterfaceItemClickListener {
            override fun onClick(view: View, position: Int, isLongClick: Boolean) {

                val goToProfile = Intent(mContext, UserProfileActivity::class.java)
                goToProfile.putExtra("UserMail", Object.mail)
                goToProfile.putExtra("Image", Object.image)

                if (Object != null) {
                    goToProfile.putExtra("LastLogin", Object.lastlogin.toString() + "")
                } else {
                    goToProfile.putExtra("LastLogin", "Never login")
                }
                if (Object.lastlogout != null) {
                    goToProfile.putExtra("LastLogout", Object.lastlogout.toString() + "")
                } else {
                    goToProfile.putExtra("LastLogout", "Never logout")
                }
                goToProfile.putExtra("isActive", Object.isActive)
                goToProfile.putExtra("uid", Object.uid)
                // goToProfile.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                goToProfile.addCategory(Intent.CATEGORY_HOME)
                goToProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                mContext.startActivity(goToProfile)
            }
        })

    }


}