package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R

class CreateTaskActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_task)


        val save = findViewById<View>(R.id.create_task_save_button) as Button

        save.setOnClickListener {
            val title = findViewById<View>(R.id.task_create_title_value) as EditText
            val desc = findViewById<View>(R.id.task_create_description) as EditText

            val bundle = intent.extras
            val uid = bundle!!.getString("selected_uid")
            val task_count = bundle.getString("task_count")

            StaticDataSingleton.databaseReference.child("users").child(uid).child("Tasks").child(task_count).child("title").setValue(title.text.toString())
            StaticDataSingleton.databaseReference.child("users").child(uid).child("Tasks").child(task_count).child("description").setValue(desc.text.toString())

            val goBack = Intent(this@CreateTaskActivity, UserProfileActivity::class.java)
            super@CreateTaskActivity.finish()
            finish()
        }
    }
}