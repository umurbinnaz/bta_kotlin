package com.example.umuryavuz.binnazteknikappkotlin.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.DisplayedLogInfo
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.LogInfo
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.ViewHolders.ViewHolderOnlineLog
import java.util.ArrayList
import java.util.HashMap

class AdapterDateRangeLogs : RecyclerView.Adapter<ViewHolderOnlineLog> {

      var online_logs: HashMap<String, ArrayList<LogInfo>>
      var logs: ArrayList<DisplayedLogInfo>

    constructor(arrayList: HashMap<String, ArrayList<LogInfo>>){
        online_logs = arrayList
        logs = ArrayList<DisplayedLogInfo>()
        for (e1 in online_logs.entries) {
            for (t in e1.value) {
                val temp = DisplayedLogInfo()
                // temp.setDate(e1.getKey());
                temp.lastlogin = t.login
                temp.lastlogout = t.logout
                temp.totalworktime = t.elapsed
                logs.add(temp)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderOnlineLog {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_temp_log, parent, false)
        return ViewHolderOnlineLog(view)
    }

    override fun getItemCount(): Int {
        var size = 0
        for ((_, value) in online_logs) {
            for (t in value) {
                size++
            }
        }
        return size
    }

    override fun onBindViewHolder(holder: ViewHolderOnlineLog, position: Int) {
        val Object = logs[position]

        holder.setFirstLogin(Object.lastlogin)
        holder.setLastLogout(Object.lastlogout)
        holder.setWorkTime(Object.totalworktime)

        if (position == itemCount) {
            logs.clear()
        }
    }

}