package com.example.umuryavuz.binnazteknikappkotlin.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.umuryavuz.binnazteknikappkotlin.Fragments.FragmentDateRangeLogs
import com.example.umuryavuz.binnazteknikappkotlin.Fragments.FragmentDisplayedLogs

class AdapterAdminAccountViewPager(fm: FragmentManager, internal var mNumOfTabs: Int) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {

        when (position) {
            0 -> {

                return FragmentDisplayedLogs()
            }

            1 -> {

                return FragmentDateRangeLogs()
            }

            else -> return null
        }

    }

    override fun getCount(): Int {
        return mNumOfTabs
    }
}