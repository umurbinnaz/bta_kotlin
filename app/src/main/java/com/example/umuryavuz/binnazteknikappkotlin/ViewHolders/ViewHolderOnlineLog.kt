package com.example.umuryavuz.binnazteknikappkotlin.ViewHolders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.R
import java.util.*

class ViewHolderOnlineLog(itemView : View) : RecyclerView.ViewHolder(itemView) {

    lateinit var date : TextView
    lateinit var last_login : TextView
    lateinit var last_logout : TextView
    lateinit var total_work : TextView

    fun setFirstLogin(firstLogin : Long?){
        last_login = itemView.findViewById(R.id.selected_first_login_temp) as TextView
        if(firstLogin != null){
            val ts = Date(firstLogin)
            val c : Calendar? = null
            if (c != null) {
                c.time = ts
                last_login.text = c.get(Calendar.HOUR).toString() + ":" + c.get(Calendar.MINUTE).toString()
            }
        }
    }

    fun setLastLogout(lastLogout: Long?) {
        last_logout = itemView.findViewById(R.id.selected_last_logout_temp) as TextView
        if (lastLogout != null) {
            val ts = Date(lastLogout)
            val c: Calendar? = null
            if (c != null) {
                c.time = ts
                last_logout.text = c.get(Calendar.HOUR).toString() + ":" + c.get(Calendar.MINUTE)
            }
        } else {
            last_logout.setText(R.string.log_open)
        }
    }

    fun setDate(logNumber: String) {
        date = itemView.findViewById(R.id.date_temp) as TextView
        date.text = logNumber
    }

    fun setWorkTime(workTime: Long?) {
        total_work = itemView.findViewById(R.id.selected_work_time_temp) as TextView
        if (workTime != null) {
            total_work.text = Math.ceil(workTime as Double / 60).toString()
        } else {
            total_work.setText(R.string.log_open)
        }
    }


}