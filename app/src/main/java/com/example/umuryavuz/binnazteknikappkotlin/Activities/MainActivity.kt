package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var bar : ProgressDialog
    lateinit var valueEventListener: ValueEventListener
    lateinit var authStateListener: FirebaseAuth.AuthStateListener
    lateinit var bar2: ProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        userLogin.setOnClickListener{
            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
        }
        bar2 = ProgressDialog(this)
        bar = ProgressDialog(this)
        bar.setMessage("Retrieving current account")
        bar.setCancelable(false)
        bar.show()


        valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                if (dataSnapshot.hasChild(StaticDataSingleton.getUserId()!!)) {

                    Crashlytics.setUserEmail(StaticDataSingleton.getUser()!!.getEmail())
                    Crashlytics.setUserName(dataSnapshot.child(StaticDataSingleton.getUserId()!!).child("name").getValue(String::class.java))
                    Crashlytics.setUserIdentifier(StaticDataSingleton.getUserId())

                    if (dataSnapshot.child(StaticDataSingleton.getUserId()!!).child("roles").child("administrator").getValue(Boolean::class.javaPrimitiveType!!)!!) {
                        if ((!dataSnapshot.child(StaticDataSingleton.getUserId()!!).child("isActive").getValue(Boolean::class.javaPrimitiveType!!)!!)) {
                            StaticDataSingleton.mAuth.signOut()

                            Toast.makeText(this@MainActivity, "Your Account has not been activated!!", Toast.LENGTH_LONG).show()

                            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                            bar2.dismiss()
                            bar.dismiss()
                        }
                        else {
                            bar2.dismiss()
                            bar.dismiss()
                            startActivity(Intent(this@MainActivity, AdminAccountActivity::class.java))
                        }
                        StaticDataSingleton.getmAuth()!!.removeAuthStateListener(authStateListener)
                        StaticDataSingleton.DatabaseReference().child("users").removeEventListener(valueEventListener)
                        Log.d("MAIN ACTV", "START");
                        finish()

                    }
                    else {

                        if ((!dataSnapshot.child(StaticDataSingleton.getUserId()!!).child("isActive").getValue(Boolean::class.javaPrimitiveType!!)!!)) {
                            StaticDataSingleton.mAuth.signOut()
                            Toast.makeText(this@MainActivity, "Your Account has not been activated!!", Toast.LENGTH_LONG).show()
                            val toLogin = Intent(this@MainActivity, LoginActivity::class.java)
                            toLogin.addCategory(Intent.CATEGORY_HOME)
                            toLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(toLogin)
                            bar2.dismiss()
                            bar.dismiss()
                        }
                        else {
                            val toLogin = Intent(this@MainActivity, UserAccountActivity::class.java)
                            toLogin.addCategory(Intent.CATEGORY_HOME)
                            toLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(toLogin)
                            bar2.dismiss()
                            bar.dismiss()
                        }
                        finish()
                    }
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("FIREBASE", databaseError.message)
            }
        }
        authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser != null) {
                bar2 = ProgressDialog(this@MainActivity)
                bar2.setMessage("Please wait")
                bar2.setCancelable(false)
                bar2.show()
                StaticDataSingleton.DatabaseReference().child("users").addValueEventListener(valueEventListener)
            } else {
                bar.dismiss()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        if (!StaticDataSingleton.is_login) {
            StaticDataSingleton.getmAuth()!!.addAuthStateListener(authStateListener)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        StaticDataSingleton.getmAuth()!!.removeAuthStateListener(authStateListener)
        StaticDataSingleton.DatabaseReference().child("users").removeEventListener(valueEventListener)
    }
}
