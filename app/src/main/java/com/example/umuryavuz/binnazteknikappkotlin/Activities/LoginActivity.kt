package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(){

    lateinit var mLoginbtn: Button
    lateinit var mUsername: EditText
    lateinit var mPassword: EditText
    lateinit var mReset_pass: TextView
    lateinit var mAuth: FirebaseAuth
    lateinit var databaseReference: DatabaseReference
    lateinit var progressDialog: ProgressDialog
    var currentUser: FirebaseUser? = null
    var isactive: Boolean = false




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        mAuth = FirebaseAuth.getInstance()
        mLoginbtn = loginbtn
        mUsername = username
        mPassword = password
        mReset_pass = reset_password

        StaticDataSingleton.is_login = true
        databaseReference = FirebaseDatabase.getInstance().reference
        progressDialog = ProgressDialog(this)
        if(::mLoginbtn.isInitialized) {
            mLoginbtn.setOnClickListener {
                progressDialog.setCancelable(false)
                progressDialog.setMessage("Loging")
                progressDialog.show()
                checkLogin()
            }
        }
        if(::mReset_pass.isInitialized) {
            mReset_pass.setOnClickListener {
                if (!TextUtils.equals(mUsername.text, "")) {
                    mAuth.sendPasswordResetEmail(mUsername.text.toString())
                    Toast.makeText(this@LoginActivity, "Reset mail is sent to usermail", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this@LoginActivity, "Please enter usermail!", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun checkLogin() {

        val username = mUsername.text.toString().trim { it <= ' ' }
        val password = mPassword.text.toString().trim { it <= ' ' }

        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            StaticDataSingleton.mAuth.signInWithEmailAndPassword(username, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    progressDialog.dismiss()
                    StaticDataSingleton.is_login = false

                }

                else {

                    progressDialog.dismiss()
                    Toast.makeText(this@LoginActivity, "Username or Password is incorrect, please try again!", Toast.LENGTH_LONG).show()

                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = StaticDataSingleton.getUser()
    }

    override fun onPause() {
        super.onPause()
        super@LoginActivity.finish()
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}