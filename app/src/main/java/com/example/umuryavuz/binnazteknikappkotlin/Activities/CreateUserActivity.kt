package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.Mail.SendMailTask
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import org.apache.commons.lang3.RandomStringUtils
import java.security.SecureRandom
import java.util.*

class CreateUserActivity :  AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var createUser: Button? = null
    private var name: EditText? = null
    private var surname: EditText? = null
    private var mail: EditText? = null
    private var bar: ProgressDialog? = null
    private var mAuth: FirebaseAuth? = null
    private var adminMail: String? = null
    private var context: Context? = null
    private var page_header: ImageView? = null
    private var mAuth1: FirebaseAuth? = null
    private var mAuth2: FirebaseAuth? = null
    private var dept1: CheckBox? = null
    private var dept2: CheckBox? = null
    private var dept3: CheckBox? = null
    private var dept4: CheckBox? = null
    private var dept5: CheckBox? = null
    private var dept6: CheckBox? = null
    private var dept_count: Int = 0
    private var dept_ids: ArrayList<String>? = null
    private var dept_ch: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)


        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.elevation = 0f


        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }


        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()
        toggle.isDrawerIndicatorEnabled = false
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        toolbar.setNavigationOnClickListener { finish() }

        context = applicationContext



        mAuth = FirebaseAuth.getInstance()

        createUser = findViewById<View>(R.id.Create) as Button

        name = findViewById<View>(R.id.create_user_name) as EditText
        surname = findViewById<View>(R.id.create_user_surname) as EditText
        mail = findViewById<View>(R.id.create_mail) as EditText

        bar = ProgressDialog(this)

        adminMail = mAuth!!.currentUser!!.email

        page_header = findViewById<View>(R.id.create_user_background_image) as ImageView

        dept1 = findViewById<View>(R.id.dept_1) as CheckBox
        dept2 = findViewById<View>(R.id.dept_2) as CheckBox
        dept3 = findViewById<View>(R.id.dept_3) as CheckBox
        dept4 = findViewById<View>(R.id.dept_4) as CheckBox
        dept5 = findViewById<View>(R.id.dept_5) as CheckBox
        dept6 = findViewById<View>(R.id.dept_6) as CheckBox


        dept1!!.setOnClickListener {
            if (dept1!!.isChecked) {
                dept2!!.isChecked = false
                dept3!!.isChecked = false
                dept4!!.isChecked = false
                dept5!!.isChecked = false
                dept6!!.isChecked = false
            }
        }
        dept2!!.setOnClickListener {
            if (dept2!!.isChecked) {
                dept1!!.isChecked = false
                dept3!!.isChecked = false
                dept4!!.isChecked = false
                dept5!!.isChecked = false
                dept6!!.isChecked = false
            }
        }
        dept3!!.setOnClickListener {
            if (dept3!!.isChecked) {
                dept2!!.isChecked = false
                dept1!!.isChecked = false
                dept4!!.isChecked = false
                dept5!!.isChecked = false
                dept6!!.isChecked = false
            }
        }
        dept4!!.setOnClickListener {
            if (dept4!!.isChecked) {
                dept2!!.isChecked = false
                dept3!!.isChecked = false
                dept1!!.isChecked = false
                dept5!!.isChecked = false
                dept6!!.isChecked = false
            }
        }
        dept5!!.setOnClickListener {
            if (dept5!!.isChecked) {
                dept1!!.isChecked = false
                dept2!!.isChecked = false
                dept3!!.isChecked = false
                dept4!!.isChecked = false
                dept6!!.isChecked = false
            }
        }
        dept6!!.setOnClickListener {
            if (dept6!!.isChecked) {
                dept1!!.isChecked = false
                dept2!!.isChecked = false
                dept3!!.isChecked = false
                dept4!!.isChecked = false
                dept5!!.isChecked = false
            }
        }






        StaticDataSingleton.DatabaseReference().child("users").child(mAuth!!.currentUser!!.uid).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val background_image_url = dataSnapshot.child("background_image").getValue(String::class.java)

                if (background_image_url != null) {
                    Glide.with(applicationContext).load(background_image_url).into(page_header!!)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })


        mAuth1 = FirebaseAuth.getInstance()


        val firebaseOptions = FirebaseOptions.Builder()
                .setDatabaseUrl("https://binnazteknikappkotlin.firebaseio.com/")
                .setApiKey("AIzaSyD_k8G5thowVVtO-E734jm25tEY0QZA5dY")
                .setApplicationId("binnazteknikappkotlin").build()

        try {
            val myApp = FirebaseApp.initializeApp(applicationContext, firebaseOptions, "AnyAppName")
            mAuth2 = FirebaseAuth.getInstance(myApp)
        } catch (e: IllegalStateException) {
            mAuth2 = FirebaseAuth.getInstance(FirebaseApp.getInstance("AnyAppName"))
        }


        StaticDataSingleton.DatabaseReference().child("users").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //  is_admin = dataSnapshot.child(mAuth.getCurrentUser().getUid()).child("roles").child("administrator").getValue(boolean.class);
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

        createUser!!.setOnClickListener { startPostData() }


    }


    private fun startPostData() {
        val u_mail = mail!!.text.toString().trim { it <= ' ' }
        val u_name = name!!.text.toString().trim { it <= ' ' }
        val u_surname = surname!!.text.toString().trim { it <= ' ' }

        dept_ids = ArrayList()
        dept_ch = ArrayList()

        var is_dept_checked = false
        dept_count = 0
        if (dept1!!.isChecked) {
            dept_ids!!.add("Faladdin Teknik")
            dept_ch!!.add("teknik-genel")
        }

        if (dept2!!.isChecked) {
            dept_ids!!.add("Binnaz Teknik")
            dept_ch!!.add("teknik-genel")
        }

        if (dept3!!.isChecked) {
            dept_ids!!.add("Binnaz Müşteri Hizmetleri")
            dept_ch!!.add("teknik-genel")
        }

        if (dept4!!.isChecked) {
            dept_ids!!.add("Faladdin Müşteri Hizmetleri")
            dept_ch!!.add("teknik-genel")
        }

        if (dept5!!.isChecked) {
            dept_ids!!.add("Yorumcu İlişkileri")
            dept_ch!!.add("teknik-genel")
        }

        if (dept6!!.isChecked) {
            dept_ids!!.add("Bizim Akademi")
            dept_ch!!.add("bizim-akademi")
        }

        if (!dept1!!.isChecked && !dept2!!.isChecked && !dept3!!.isChecked && !dept4!!.isChecked && !dept5!!.isChecked && !dept6!!.isChecked) {
            is_dept_checked = false
        } else {
            is_dept_checked = true
        }

        if (!TextUtils.isEmpty(u_mail) && is_dept_checked) {
            bar!!.setMessage("Creating")
            bar!!.setCancelable(false)
            bar!!.show()

            val possibleCharacters = "0123456789".toCharArray()
            val randomStr = RandomStringUtils.random(8, 0, possibleCharacters.size - 1, false, false, possibleCharacters, SecureRandom())

            mAuth2!!.createUserWithEmailAndPassword(u_mail, randomStr).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val newUser = StaticDataSingleton.DatabaseReference().child("users").child(mAuth2!!.currentUser!!.uid)
                    newUser.child("name").setValue(u_name)
                    newUser.child("secondname").setValue(u_surname)
                    newUser.child("mail").setValue(u_mail)
                    newUser.child("roles").child("administrator").setValue(false)
                    newUser.child("online").setValue(false)
                    newUser.child("uid").setValue(mAuth2!!.currentUser!!.uid)
                    newUser.child("isActive").setValue(true)
                    newUser.child("department").child("name").setValue(dept_ids!![0])
                    newUser.child("department").child("channel").setValue(dept_ch!![0])

                    Toast.makeText(this@CreateUserActivity, "Registration successful",
                            Toast.LENGTH_SHORT).show()
                    mAuth2!!.signOut()
                    bar!!.dismiss()

                    val mailList = Arrays.asList(u_mail)
                    SendMailTask(this@CreateUserActivity).execute("binnazteknikapp@gmail.com",
                            "Nesnemarket32", mailList, "New User", "Usermail: $u_mail Password: $randomStr")

                    //
                } else {
                    bar!!.dismiss()
                    Toast.makeText(this@CreateUserActivity, "Error at Registration!", Toast.LENGTH_LONG).show()
                    Log.e("Error At Registration", task.exception.toString())
                }


                val toAdmin = Intent(this@CreateUserActivity, AdminAccountActivity::class.java)
                //toAdmin.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                toAdmin.addCategory(Intent.CATEGORY_HOME)
                toAdmin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toAdmin)
            }
        } else {
            if (is_dept_checked) {
                Toast.makeText(this@CreateUserActivity, "Please fill the empty spaces!", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this@CreateUserActivity, "Please select a department!", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_logout) {
            val ref = FirebaseDatabase.getInstance().reference.child("users").child(mAuth!!.currentUser!!.uid).child("online")
            ref.setValue(false)
            bar!!.setMessage("Logging out")
            bar!!.show()
            if (StaticDataSingleton.getUser() != null) {

                //StaticDataSingleton.getInstance().getDatabaseReference().child("users").child(StaticDataSingleton.getInstance().getUserId()).child("currentDayNumber").setValue(dayNumber_ts);
            }
            super@CreateUserActivity.finish()
            finish()
            mAuth!!.signOut()
        } else if (id == R.id.nav_edit_profile) {
            val toEdit = Intent(this@CreateUserActivity, EditProfileActivity::class.java)
            //toEdit.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toEdit.addCategory(Intent.CATEGORY_HOME)
            toEdit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toEdit)
        } else if (id == R.id.nav_create_user) {
            val toCreate = Intent(this@CreateUserActivity, CreateUserActivity::class.java)
            //toCreate.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toCreate.addCategory(Intent.CATEGORY_HOME)
            toCreate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toCreate)
        } else if (id == R.id.nav_list_users) {
            val toInterface = Intent(this@CreateUserActivity, UserListActivity::class.java)
            //toInterface.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toInterface.addCategory(Intent.CATEGORY_HOME)
            toInterface.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toInterface)
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }


}