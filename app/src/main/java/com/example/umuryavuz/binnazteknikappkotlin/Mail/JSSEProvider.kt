package com.example.umuryavuz.binnazteknikappkotlin.Mail

import java.security.AccessController
import java.security.PrivilegedAction
import java.security.Provider

internal class JSSEProvider : Provider{

    constructor() : super("HarmonyJSSE", 1.0, "Harmony JSSE Provider"){
        AccessController.doPrivileged(PrivilegedAction<Void> {
            put("SSLContext.TLS",
                    "org.apache.harmony.xnet.provider.jsse.SSLContextImpl")
            put("Alg.Alias.SSLContext.TLSv1", "TLS")
            put("KeyManagerFactory.X509",
                    "org.apache.harmony.xnet.provider.jsse.KeyManagerFactoryImpl")
            put("TrustManagerFactory.X509",
                    "org.apache.harmony.xnet.provider.jsse.TrustManagerFactoryImpl")
            null
        })
    }
}