package com.example.umuryavuz.binnazteknikappkotlin.ViewHolders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ViewHolderDisplayedLogInfo(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var date: TextView
    lateinit var last_login: TextView
    lateinit var last_logout: TextView
    lateinit var total_work: TextView
    lateinit var log_index: TextView

    fun setFirstLogin(firstLogin: Long?) {
        last_login = itemView.findViewById<View>(R.id.selected_first_login_temp) as TextView
        if (firstLogin != null) {
            try {
                last_login.setText(StaticDataSingleton.to24Hours(firstLogin))
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }
    }

    fun setLastLogout(lastLogout: Long?) {
        last_logout = itemView.findViewById<View>(R.id.selected_last_logout_temp) as TextView
        if (lastLogout != null) {
            try {
                last_logout.setText(StaticDataSingleton.to24Hours(lastLogout))
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        } else {
            last_logout.setText(R.string.log_open)
        }
    }

    fun setDate(logNumber: Long?) {
        date = itemView.findViewById<View>(R.id.date_temp) as TextView

        if (logNumber != null) {
            val temp_date = Date(logNumber)

            val temp_cal = Calendar.getInstance()
            temp_cal.time = temp_date
            temp_cal.add(Calendar.MONTH, 1)
            date.text = temp_cal.get(Calendar.YEAR).toString() + "/" + (if (temp_cal.get(Calendar.MONTH) > 10) temp_cal.get(Calendar.MONTH) else "0" + temp_cal.get(Calendar.MONTH)) + "/" + (if (temp_cal.get(Calendar.DAY_OF_MONTH) > 10) temp_cal.get(Calendar.DAY_OF_MONTH) else "0" + temp_cal.get(Calendar.DAY_OF_MONTH)) + "  " + getDay(temp_cal.get(Calendar.DAY_OF_MONTH), logNumber)
        } else {
            date.setText(R.string.log_open)
        }
    }

    fun setWorkTime(workTime: Long?) {
        total_work = itemView.findViewById<View>(R.id.selected_work_time_temp) as TextView
        if (workTime != null) {
            total_work.text = Math.floor((workTime / 60).toDouble()).toInt().toString() + " min"
        } else {
            total_work.setText(R.string.log_open)
        }
    }

    fun setLogIndex(index: Int) {
        log_index = itemView.findViewById<View>(R.id.log_index) as TextView
        log_index.text = "Log $index"
    }

    fun getDay(day: Int, t: Long?): String {
        val target_Cal = Calendar.getInstance()
        target_Cal.time = Date(t!!)
        val target_time = target_Cal.time

        TimeZone.setDefault(TimeZone.getTimeZone("GMT+03"))

        target_time.date = day


        val sdf = SimpleDateFormat("EEEE")
        return sdf.format(target_time)
    }
}
