package com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.SystemClock
import android.support.v4.app.NotificationCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Button
import android.widget.Chronometer
import android.widget.RemoteViews
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.Adapter.AdapterDisplayedLogInfo
import com.example.umuryavuz.binnazteknikappkotlin.Activities.AdminAccountActivity
import com.example.umuryavuz.binnazteknikappkotlin.Activities.NotificationBroadcastReceiver
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.Activities.UserAccountActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

@SuppressLint("StaticFieldLeak")
object StaticDataSingleton {

    lateinit var mAuth: FirebaseAuth
    lateinit var firebaseDatabase: FirebaseDatabase
    lateinit var databaseReference: DatabaseReference
    var is_Admin: Boolean = false
    var is_login: Boolean = false
    var temp_time: Long = 0
    lateinit var year: String
    lateinit var month: String
    lateinit var time: String
    var notificationManager: NotificationManager? = null
    var builder: NotificationCompat.Builder? = null
    lateinit var mContentView: RemoteViews
    lateinit var chronometer: Chronometer
    lateinit var notification: Notification
    lateinit var data: JSONObject
    lateinit  var slackRequest: SlackRequest

    private var authStateListener: FirebaseAuth.AuthStateListener = FirebaseAuth.AuthStateListener { }


    fun getIs_Admin(): Boolean {
        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                is_Admin = dataSnapshot.child("users").child(getUserId()!!).child("roles").child("administrator").getValue(Boolean::class.java)!!
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
        return is_Admin
    }


    fun getmAuth(): FirebaseAuth? {
        if (!::mAuth.isInitialized) {
            mAuth = FirebaseAuth.getInstance()
            mAuth.addAuthStateListener(authStateListener)

        }
        return mAuth
    }

    fun getUser(): FirebaseUser? {
        getmAuth()
         if(::mAuth.isInitialized) {
             return mAuth.currentUser
        }
        return null
    }

    fun  getUserId(): String? {
        getmAuth()
        return if (::mAuth.isInitialized) {
            mAuth.currentUser!!.uid
        } else {
            null
        }
    }

    fun FirebaseDatabase(): FirebaseDatabase {
        firebaseDatabase = FirebaseDatabase.getInstance()
        return firebaseDatabase
    }

    fun DatabaseReference(): DatabaseReference {
        return FirebaseDatabase().reference
    }

    fun performRecord(mContext: Context, onlineButton: Button, timeElapsed: Chronometer, login_count: Int, is_online: Boolean) {
        if (!is_online) {
            StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("CurrentTimestamp").setValue(ServerValue.TIMESTAMP)

            StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("CurrentTimestamp").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    val temp_time = dataSnapshot.getValue(Long::class.java)
                    StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("Logs").child(login_count.toString() + "").child("login").setValue(temp_time).addOnCompleteListener(OnCompleteListener<Void> { task ->
                        if (task.isSuccessful) {
                            StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onDataChange(dataSnapshot: DataSnapshot) {
                                   sendMessageSlack(dataSnapshot.child("name").getValue(String::class.java), "online", dataSnapshot.child("department").child("channel").getValue(String::class.java))
                                    //if the slack message send is succeed.
                                    if (true /*slackRequest.getResponse() == "success"*/) {

                                        if (dataSnapshot.child("Log").hasChild((login_count - 1).toString() + "")) {
                                            if (dataSnapshot.child("Log").child((login_count - 1).toString() + "").hasChild("logout")) {

                                            } else {
                                                StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("Log").child((login_count - 1).toString() + "").child("logout").setValue(temp_time)
                                            }
                                        }


                                        timeElapsed.base = SystemClock.elapsedRealtime()
                                        timeElapsed.start()


                                        if (notificationManager == null) {
                                            notificationManager = mContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                                        }


                                        val intent: Intent
                                        val pendingIntent: PendingIntent

                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                            val importance = NotificationManager.IMPORTANCE_HIGH
                                            var mChannel: NotificationChannel? = notificationManager!!.getNotificationChannel("Binnaz Teknik App")
                                            if (mChannel == null) {
                                                mChannel = NotificationChannel("Binnaz Teknik App", "Binnaz Teknik App", importance)
                                                mChannel.description = "Record Time"
                                                mChannel.enableVibration(true)
                                                mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                                                notificationManager!!.createNotificationChannel(mChannel)
                                            }


                                            val notificationIntent: Intent

                                            if (dataSnapshot.child("roles").child("administrator").getValue(Boolean::class.java)!!) {
                                                notificationIntent = Intent(mContext, AdminAccountActivity::class.java)
                                            } else {
                                                notificationIntent = Intent(mContext, UserAccountActivity::class.java)
                                            }

                                            notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

                                            val intent_home = PendingIntent.getActivity(mContext, 0,
                                                    notificationIntent, 0)

                                            intent = Intent(mContext, NotificationBroadcastReceiver::class.java)
                                            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            intent.putExtra("action", "Stop")
                                            intent.action = "Stop"
                                            pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)


                                            builder = NotificationCompat.Builder(mContext, "Binnaz Teknik App")

                                            builder!!.setContentTitle("Record Time")
                                                    .setAutoCancel(true)
                                                    .setContentIntent(null)
                                                    .setSmallIcon(R.mipmap.ic_date_range_black_24dp)
                                                    .addAction(
                                                            android.R.drawable.stat_notify_error,
                                                            "Stop",
                                                            pendingIntent)
                                                    .setOngoing(true)

                                            builder!!.setContentIntent(intent_home)
                                            builder!!.setShowWhen(false)

                                            notificationManager = mContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager


                                        }
                                        else {
                                            builder = NotificationCompat.Builder(mContext)

                                            val notificationIntent: Intent

                                            if (getIs_Admin()) {
                                                notificationIntent = Intent(mContext, AdminAccountActivity::class.java)
                                            } else {
                                                notificationIntent = Intent(mContext, UserAccountActivity::class.java)
                                            }

                                            notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

                                            val intent_home = PendingIntent.getActivity(mContext, 0,
                                                    notificationIntent, 0)

                                            intent = Intent(mContext, NotificationBroadcastReceiver::class.java)
                                            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            intent.putExtra("action", "Stop")
                                            intent.action = "Stop"
                                            pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                                            builder!!.setContentTitle("Record Time")
                                                    .setAutoCancel(true)
                                                    .setContentIntent(null)
                                                    .setSmallIcon(R.mipmap.ic_date_range_black_24dp)
                                                    .addAction(
                                                            android.R.drawable.stat_notify_error,
                                                            "Stop",
                                                            pendingIntent)
                                                    .setOngoing(true)
                                            builder!!.setContentIntent(intent_home)
                                            builder!!.setShowWhen(false)


                                        }
                                        startChronometer("", SystemClock.elapsedRealtime(), mContext)
                                        onlineButton.setBackgroundResource(R.drawable.button_red)
                                        //onlineButton.setCompoundDrawablesWithIntrinsicBounds(0, 0,  R.drawable.circle_red, 0);
                                        onlineButton.setText(R.string.stop_record)

                                        StaticDataSingleton.DatabaseReference().child("users").child(mAuth.currentUser!!.uid).child("online").setValue(true)

                                        StaticDataSingleton.DatabaseReference().child("users").child(mAuth.currentUser!!.uid).child("lastlogin").setValue(temp_time)
                                    }
                                }

                                override fun onCancelled(databaseError: DatabaseError) {

                                }
                            })
                        } else {
                            Log.e("Record Online Error", task.exception!!.toString())
                        }
                    })

                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })


        }else {
            timeElapsed.stop()
            StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("CurrentTimestamp").setValue(ServerValue.TIMESTAMP)

            StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("CurrentTimestamp").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    temp_time = dataSnapshot.getValue(Long::class.java)!!
                    StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("Logs").child(login_count.toString() + "").child("logout").setValue(temp_time).addOnCompleteListener(OnCompleteListener<Void> { task ->
                        if (task.isSuccessful) {
                            StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onDataChange(dataSnapshot: DataSnapshot) {

                                    sendMessageSlack(dataSnapshot.child("name").getValue(String::class.java), "offline", dataSnapshot.child("department").child("channel").getValue(String::class.java))

                                    if (true /*slackRequest.getResponse() == "success"*/) {


                                        val ts_login = dataSnapshot.child("Logs").child(login_count.toString() + "").child("login").getValue(Long::class.java)!!
                                        val ts_logout = dataSnapshot.child("Logs").child(login_count.toString() + "").child("logout").getValue(Long::class.java)!!
                                        val tm_elapsed = ts_logout - ts_login

                                        StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("Logs").child(login_count.toString() + "").child("elapsed").setValue(TimeUnit.MILLISECONDS.toSeconds(tm_elapsed))


                                        val tss = TimeUnit.MILLISECONDS.toMicros(getStartOfDayEpochSecond(ts_logout)!!)
                                        val tss_next = TimeUnit.MILLISECONDS.toMicros(getEndOfDayEpochSecond(ts_logout)!!)

                                        StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("LogsTimes").addListenerForSingleValueEvent(object : ValueEventListener {
                                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                                if (ts_login >= tss && ts_logout < tss_next) {
                                                    if (dataSnapshot.hasChild(tss.toString() + "")) {
                                                        var temp_time = dataSnapshot.child(tss.toString() + "").getValue(Long::class.java)!!
                                                        temp_time += TimeUnit.MILLISECONDS.toSeconds(tm_elapsed)
                                                        StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("LogsTimes").child(tss.toString() + "").setValue(temp_time)

                                                    } else {
                                                        StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("LogsTimes").child(tss.toString() + "").setValue(TimeUnit.MILLISECONDS.toSeconds(tm_elapsed))

                                                    }
                                                }
                                            }

                                            override fun onCancelled(databaseError: DatabaseError) {

                                            }
                                        })

                                        onlineButton.setBackgroundResource(R.drawable.button_green)
                                        // onlineButton.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.circle_green, 0);
                                        onlineButton.setText(R.string.start_record)

                                        StaticDataSingleton.DatabaseReference().child("users").child(mAuth.currentUser!!.uid).child("online").setValue(false)
                                        StaticDataSingleton.DatabaseReference().child("users").child(mAuth.currentUser!!.uid).child("lastlogout").setValue(temp_time)

                                        val lc = login_count + 1
                                        StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("daily_login_counter").setValue(lc)

                                        if (builder != null) {
                                            val intent: Intent
                                            val pendingIntent: PendingIntent
                                            chronometer.stop()

                                            builder = NotificationCompat.Builder(mContext)
                                            notificationManager = mContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

                                            val timeE = dataSnapshot.child("Logs").child((login_count - 1).toString() + "").child("elapsed").getValue(Int::class.java)!!
                                            val tm_elps = Math.floor((timeE / 3600).toDouble()).toInt().toString() + " hours " + Math.floor((timeE / 60).toDouble()).toInt() + " min " + timeE % 60 + " sec "

                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                                val importance = NotificationManager.IMPORTANCE_HIGH
                                                lateinit var mChannel: NotificationChannel
                                                mChannel = notificationManager!!.getNotificationChannel("Binnaz Teknik App")
                                                if (mChannel == null) {


                                                    mChannel = NotificationChannel("Binnaz Teknik App", "Binnaz Teknik App", importance)
                                                    mChannel.description = "Record Stoped  Time elapsed : $tm_elps"
                                                    mChannel.enableVibration(true)
                                                    mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                                                    notificationManager!!.createNotificationChannel(mChannel)
                                                }


                                                val notificationIntent: Intent

                                                if (dataSnapshot.child("roles").child("administrator").getValue(Boolean::class.java)!!) {
                                                    notificationIntent = Intent(mContext, AdminAccountActivity::class.java)
                                                } else {
                                                    notificationIntent = Intent(mContext, UserAccountActivity::class.java)
                                                }

                                                notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

                                                val intent_home = PendingIntent.getActivity(mContext, 0,
                                                        notificationIntent, 0)

                                                builder = NotificationCompat.Builder(mContext, "Binnaz Teknik App")

                                                builder!!.setContentTitle("Record Stoped")
                                                        .setContentText("Time elapsed : $tm_elps")
                                                        .setContentIntent(null)
                                                        .setSmallIcon(R.mipmap.ic_date_range_black_24dp)

                                                builder!!.setContentIntent(intent_home)


                                                notificationManager = mContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager


                                            } else {
                                                builder = NotificationCompat.Builder(mContext)

                                                val notificationIntent: Intent

                                                if (getIs_Admin()) {
                                                    notificationIntent = Intent(mContext, AdminAccountActivity::class.java)
                                                } else {
                                                    notificationIntent = Intent(mContext, UserAccountActivity::class.java)
                                                }

                                                notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

                                                val intent_home = PendingIntent.getActivity(mContext, 0,
                                                        notificationIntent, 0)

                                                builder!!.setContentTitle("Record Stoped")
                                                        .setContentText("Time elapsed : $tm_elps")
                                                        .setContentIntent(null)
                                                        .setSmallIcon(R.mipmap.ic_date_range_black_24dp)

                                                builder!!.setContentIntent(intent_home)
                                            }

                                            builder!!.setUsesChronometer(false)
                                            builder!!.setShowWhen(false)


                                            notification = builder!!.build()
                                            notificationManager!!.notify(1, notification)
                                        }
                                    }
                                }

                                override fun onCancelled(databaseError: DatabaseError) {

                                }
                            })
                        } else {
                            Log.e("Record Offline Error", task.exception!!.toString())
                        }
                    })

                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })


        }


    }

    fun stopAction() {
        StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("CurrentTimestamp").setValue(ServerValue.TIMESTAMP)
        StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("online").setValue(false)
                val temp = dataSnapshot.child("daily_login_counter").getValue(Int::class.java)!!
                StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("daily_login_counter").setValue(temp + 1)
                val temp_time = dataSnapshot.child("CurrentTimestamp").getValue(Long::class.java)!!
                StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("Logs").child(temp.toString() + "").child("logout").setValue(temp_time)

                val ts_login = dataSnapshot.child("Logs").child(temp.toString() + "").child("login").getValue(Long::class.java)!!
                val ts_logout = temp_time
                val tm_elapsed = ts_logout - ts_login

                StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("Logs").child(temp.toString() + "").child("elapsed").setValue(TimeUnit.MILLISECONDS.toSeconds(tm_elapsed))


                val tss = TimeUnit.MILLISECONDS.toMicros(getStartOfDayEpochSecond(ts_logout)!!)
                val tss_next = TimeUnit.MILLISECONDS.toMicros(getEndOfDayEpochSecond(ts_logout)!!)

                StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("LogsTimes").addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (ts_login >= tss && ts_logout < tss_next) {
                            if (dataSnapshot.hasChild(tss.toString() + "")) {
                                var temp_time = dataSnapshot.child(tss.toString() + "").getValue(Long::class.java)!!
                                temp_time += TimeUnit.MILLISECONDS.toSeconds(tm_elapsed)
                                StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("LogsTimes").child(tss.toString() + "").setValue(temp_time)

                            } else {
                                StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("LogsTimes").child(tss.toString() + "").setValue(TimeUnit.MILLISECONDS.toSeconds(tm_elapsed))

                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {

                    }
                })
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    fun sendMessageSlack(username: String?, message: String, channel: String?) {
         var response : Array<SlackRequest> = Array(1,{SlackRequest()})

        object : AsyncTask<Void, Void, Void>() {


            override fun onPreExecute() {
                super.onPreExecute()

            }

            override fun doInBackground(vararg params: Void): Void? {
                try {
                    val url = URL("http://34.251.151.32:8080/createLog/slackLog?msgTitle=$username&msgBody=$message&slackChannel=$channel&icon_emoji=ghost")

                    val connection = url.openConnection() as HttpURLConnection

                    val reader = BufferedReader(InputStreamReader(connection.inputStream))

                    val json = StringBuffer(1024)
                    var tmp = ""

                    while (tmp == "") {
                        tmp = reader.readLine()
                        json.append(tmp).append("\n")
                    }
                    reader.close()

                    data = JSONObject(json.toString())
                    val gson = Gson()
                    val token = object : TypeToken<SlackRequest>() {

                    }
                    response[0] = gson.fromJson<SlackRequest>(json.toString(), token.type)

                } catch (e: Exception) {

                    println("Exception " + e.message)
                    return null
                }

                return null
            }

            override fun onPostExecute(Void: Void?) {
                slackRequest = response[0]
                Log.d("Slack", "Message Sent usr: $username msg: $message ch: $channel")
                //Here actual performing should start.-
            }
        }.execute()
    }

    fun getValuesFromDate(start: Long, end: Long, total_time: TextView, recyclerView: RecyclerView) {

        Log.d("START AND END DATE", TimeUnit.MILLISECONDS.toMicros(getStartOfDayEpochSecond(start)!!).toString() + "  " + TimeUnit.MILLISECONDS.toMicros(getEndOfDayEpochSecond(end)!!))
        val start_tmp = TimeUnit.MILLISECONDS.toMicros(getStartOfDayEpochSecond(start)!!)
        val end_tmp = TimeUnit.MILLISECONDS.toMicros(getEndOfDayEpochSecond(end)!!)
        StaticDataSingleton.DatabaseReference().child("users").child(getUserId()!!).child("Logs").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val temp = ArrayList<LogInfo>()
                var total_w_t: Long = 0
                for (ds_logs in dataSnapshot.children) {
                    if (ds_logs.hasChild("login") && ds_logs.hasChild("logout")) {
                        val login = ds_logs.child("login").getValue(Long::class.java)!!
                        val logout = ds_logs.child("logout").getValue(Long::class.java)!!
                        if (login >= start_tmp && logout <= end_tmp) {
                            temp.add(ds_logs.getValue<LogInfo>(LogInfo::class.java!!)!!)
                            if (ds_logs.hasChild("elapsed")) {
                                total_w_t += ds_logs.child("elapsed").getValue(Long::class.java)!!
                            }
                        }
                    } else {
                        val login = ds_logs.child("login").getValue(Long::class.java)!!
                        if (login >= start_tmp) {
                            temp.add(ds_logs.getValue<LogInfo>(LogInfo::class.java!!)!!)
                            if (ds_logs.hasChild("elapsed")) {
                                total_w_t += ds_logs.child("elapsed").getValue(Long::class.java)!!
                            }
                        }
                    }

                    val seconds = (TimeUnit.SECONDS.toMillis(total_w_t) / 1000).toInt() % 60
                    val minutes = (TimeUnit.SECONDS.toMillis(total_w_t) / (1000 * 60) % 60).toInt()
                    val hours = (TimeUnit.SECONDS.toMillis(total_w_t) / (1000 * 60 * 60) % 24).toInt()

                    total_time.text = hours.toString() + " hour " + minutes + " minutes"

                    val mAdapter = AdapterDisplayedLogInfo(temp)
                    recyclerView.adapter = mAdapter
                }


            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })


    }

    fun getDay(day: String, current: Long?): String {
        val target_Cal = Calendar.getInstance()
        target_Cal.time = Date(current!!)
        val target_time = target_Cal.time

        TimeZone.setDefault(TimeZone.getTimeZone("GMT+03"))

        target_time.date = Integer.parseInt(day)


        val sdf = SimpleDateFormat("EEEE")
        return sdf.format(target_time)
    }

    fun getStartOfDayEpochSecond(current: Long?): Long? {
        val secondInaDay = (60 * 60 * 24).toLong()
        val currentMilliSecond = current!! / 1000
        return currentMilliSecond - currentMilliSecond % secondInaDay
    }

    fun getEndOfDayEpochSecond(current: Long?): Long? {
        val startOfTheDayEpoch = getStartOfDayEpochSecond(current)!!
        val secondInaDay = (60 * 60 * 24).toLong()
        return startOfTheDayEpoch + secondInaDay - 1
    }

    fun startChronometer(elapsedTime: String, baseTime: Long, context: Context) {

        builder!!.setUsesChronometer(true)
        builder!!.setShowWhen(false)


        mContentView = RemoteViews(context.packageName, R.layout.chronometer)
        chronometer = Chronometer(context)
        chronometer.text = elapsedTime
        chronometer.id = R.id.chronomete
        chronometer.start()



        mContentView!!.setChronometer(chronometer.id, baseTime, null, true)
        //builder.setCustomHeadsUpContentView(mContentView);


        notification = builder!!.build()
        notificationManager!!.notify(1, notification)
    }

    @Throws(ParseException::class)
    fun to24Hours(time: Long?): String {
        val d = Date(time!!)
        val now = SimpleDateFormat("hh:mm aa").format(d.time)

        val inFormat = SimpleDateFormat("hh:mm aa")
        val outFormat = SimpleDateFormat("HH:mm")

        return outFormat.format(inFormat.parse(now))
    }
}