package com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects

class LogInfo {

    var login: Long? = null

    var logout: Long? = null

    var elapsed: Long? = null

    constructor(login: Long?, logout: Long?, elapsed: Long?) {
        this.login = login
        this.logout = logout
        this.elapsed = elapsed

    }

    constructor() {}
}