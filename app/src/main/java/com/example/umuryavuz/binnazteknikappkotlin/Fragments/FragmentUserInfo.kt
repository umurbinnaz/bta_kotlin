package com.example.umuryavuz.binnazteknikappkotlin.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.Activities.UserProfileActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.text.ParseException

class FragmentUserInfo : Fragment() {

    lateinit var uid: String
    lateinit var rootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_user_profile_info, container, false)

        val bundle = UserProfileActivity.classBundle

        val usermail = bundle.getString("UserMail")!!.toString()
        val image = bundle.getString("Image")!!.toString()
        val LastLogin = bundle.getString("LastLogin")!!.toString()
        val LastLogout = bundle.getString("LastLogout")!!.toString()
        uid = bundle.getString("uid")!!.toString()
        val isActive = bundle.getBoolean("isActive")

        val mail = rootView.findViewById(R.id.user_profile_usermail) as TextView
        val login = rootView.findViewById(R.id.user_profile_last_login) as TextView
        val logout = rootView.findViewById(R.id.user_profile_last_logout) as TextView
        val worker_type = rootView.findViewById(R.id.user_profile_worker_type) as TextView

        if (LastLogin != "Never login") {
            val login_tmps = java.lang.Long.parseLong(LastLogin)
            try {
                login.setText(StaticDataSingleton.to24Hours(login_tmps))
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        } else {
            login.setText(LastLogin)
        }
        if (LastLogout != "Never logout") {
            val logout_tmps = java.lang.Long.parseLong(LastLogout)
            try {
                logout.setText(StaticDataSingleton.to24Hours(logout_tmps))
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        } else {
            logout.setText(LastLogout)
        }


        mail.setText(usermail)


        StaticDataSingleton.DatabaseReference().child("users").child(uid).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.hasChild("worker_type") || !dataSnapshot.hasChild("work_time_limit")) {
                    worker_type.visibility = View.INVISIBLE
                } else {
                    worker_type.visibility = View.VISIBLE
                    worker_type.text = dataSnapshot.child("worker_type").getValue(String::class.java)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
        return rootView
    }

}