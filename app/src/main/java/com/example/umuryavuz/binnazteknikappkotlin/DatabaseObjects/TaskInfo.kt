package com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects

class TaskInfo {
    lateinit var title: String
    lateinit var description: String
    var isIs_done: Boolean = false

    constructor(title: String, description: String, is_done: Boolean) {
        this.title = title
        this.description = description
        this.isIs_done = is_done
    }

    constructor() {}
}

