package com.example.umuryavuz.binnazteknikappkotlin.ViewHolders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.R
import java.util.*

class ViewHolderSelectedUserProfileInfo(var mView: View) : RecyclerView.ViewHolder(mView) {

    fun setFirstLogin(firstLogin: Long?) {
        val first_login = mView.findViewById<View>(R.id.selected_first_login) as TextView
        if (firstLogin != null) {
            val ts = Date(firstLogin)
            val c = Calendar.getInstance()
            c.time = ts

            first_login.text = (if (c.get(Calendar.HOUR) > 10) c.get(Calendar.HOUR) else "0" + c.get(Calendar.HOUR)).toString() + ":" + if (c.get(Calendar.MINUTE) > 10) c.get(Calendar.MINUTE) else "0" + c.get(Calendar.MINUTE)
        }
    }

    fun setLastLogout(lastLogout: Long?) {
        val last_logout = mView.findViewById<View>(R.id.selected_last_logout) as TextView
        if (lastLogout != null) {
            val ts = Date(lastLogout)
            val c = Calendar.getInstance()
            c.time = ts
            last_logout.text = (if (c.get(Calendar.HOUR) > 10) c.get(Calendar.HOUR) else "0" + c.get(Calendar.HOUR)).toString() + ":" + if (c.get(Calendar.MINUTE) > 10) c.get(Calendar.MINUTE) else "0" + c.get(Calendar.MINUTE)
        } else {
            last_logout.setText(R.string.log_open)
        }
    }

    fun setLogNumber(logNumber: String) {
        val logN = mView.findViewById<View>(R.id.selected_log_number) as TextView
        logN.text = logNumber
    }

    fun setWorkTime(workTime: Long?) {
        val workT = mView.findViewById<View>(R.id.selected_work_time) as TextView
        if (workTime != null) {
            workT.text = Math.ceil(workTime as Double / 60).toString() + " min"
        } else {
            workT.setText(R.string.log_open)
        }
    }
}