package com.example.umuryavuz.binnazteknikappkotlin.Interfaces

import android.view.View

interface InterfaceItemClickListener {
    abstract fun onClick(view: View, position: Int, isLongClick: Boolean)
}