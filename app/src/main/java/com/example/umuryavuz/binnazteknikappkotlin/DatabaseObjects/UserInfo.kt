package com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects

import java.util.ArrayList

class UserInfo {
     var mail:String? = null
     var online:Boolean = false

     var lastlogin:Long? = null
     var lastlogout:Long? = null

     var isActive:Boolean = false
     var uid:String? = null
     var name:String? = null
     var secondname:String? = null

     var departments:List<String> = ArrayList()
     var image:String? = null
         get() = if (field == null)
         {
            "https://firebasestorage.googleapis.com/v0/b/test1-dd82b.appspot.com/o/User_Images%2FNo-Image.jpg?alt=media&token=faec0662-40fb-410d-89ba-d2856f98958a"
         } else field





         constructor(departments:List<String>, mail:String, online:Boolean, lastlogin:Long?, lastlogout:Long?, image:String, isActive:Boolean, uid:String, name:String, secondname:String) {
                this.departments = departments
                this.name = name
                this.secondname = secondname
                this.mail = mail
                this.online = online
                this.lastlogin = lastlogin
                this.lastlogout = lastlogout
                this.image = image
                this.isActive = isActive
                this.uid = uid
        }

        constructor() {}
}
