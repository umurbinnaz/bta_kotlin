package com.example.umuryavuz.binnazteknikappkotlin.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.Activities.UserProfileActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener


class FragmentOptions : Fragment() {

    lateinit var activation_error_text: TextView

    lateinit var activation_button: Button

    lateinit var rootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            rootView = inflater.inflate(R.layout.fragment_user_profile_options, container, false)
        }
        activation_error_text = rootView.findViewById(R.id.activation_error_text_field) as TextView
        activation_button = rootView.findViewById(R.id.user_profile_activation_button) as Button
        activation_button.setOnClickListener(View.OnClickListener {
            if (activation_button.getText().toString() === "Activate Account") {
                StaticDataSingleton.DatabaseReference().child("users").child(UserProfileActivity.uid).child("isActive").setValue(true)
            } else if (activation_button.getText().toString() === "Disactivate Account") {
                StaticDataSingleton.DatabaseReference().child("users").child(UserProfileActivity.uid).child("isActive").setValue(false)
            }
        })

        StaticDataSingleton.DatabaseReference().child("users").child(UserProfileActivity.uid).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.child("isActive").getValue(Boolean::class.javaPrimitiveType)!!) {
                    activation_button.setText(R.string.disactivate_account)
                } else {
                    activation_button.setText(R.string.activate_account)
                }
                if (!dataSnapshot.hasChild("worker_type") || !dataSnapshot.hasChild("work_time_limit")) {
                    activation_button.setEnabled(false)
                    activation_button.setVisibility(View.INVISIBLE)
                    activation_error_text.setVisibility(View.VISIBLE)
                } else {
                    activation_button.setEnabled(true)
                    activation_button.setVisibility(View.VISIBLE)
                    activation_error_text.setVisibility(View.INVISIBLE)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
        return rootView
    }
}