package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.Spinner
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso

class EditProfileActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener{
    private var selectImage: ImageButton? = null
    private val GALLERY_REQUEST = 1
    var imageUri: Uri? = null
    private var firebaseStorage: FirebaseStorage? = null
    private var firebaseDatabase: FirebaseDatabase? = null
    private var progressDialog: ProgressDialog? = null
    private var mAuth: FirebaseAuth? = null
    private var authStateListener: FirebaseAuth.AuthStateListener? = null
    private var bar: ProgressDialog? = null
    private var save: Button? = null
    private var select_background: Button? = null
    private var is_admin: Boolean = false
    private var worker_type_select: Spinner? = null
    private var freelancer_work_time: Spinner? = null
    private var is_background_image: Boolean = false
    private var is_profile_image: Boolean = false
    private var navigationView: NavigationView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.elevation = 0f

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()
        toggle.isDrawerIndicatorEnabled = false
        navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView!!.setNavigationItemSelectedListener(this)
        toolbar.setNavigationOnClickListener { finish() }

        selectImage = findViewById<View>(R.id.selectImage) as ImageButton
        firebaseDatabase = FirebaseDatabase.getInstance()
        firebaseStorage = FirebaseStorage.getInstance()
        mAuth = FirebaseAuth.getInstance()
        bar = ProgressDialog(this)
        progressDialog = ProgressDialog(this)
        save = findViewById<View>(R.id.save_edit) as Button
        select_background = findViewById<View>(R.id.select_background_image) as Button
        worker_type_select = findViewById<View>(R.id.worker_tpye_select) as Spinner
        freelancer_work_time = findViewById<View>(R.id.freelancer_work_time_select) as Spinner


        firebaseDatabase!!.reference.child("users").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                if (dataSnapshot.child(mAuth!!.currentUser!!.uid).child("roles").child("administrator").getValue(Boolean::class.javaPrimitiveType)!!) {
                    is_admin = true
                    navigationView!!.inflateMenu(R.menu.activity_admin_drawer)

                }

                else {
                    is_admin = false
                    navigationView!!.inflateMenu(R.menu.activity_user_account_drawer)
                }

                if (!TextUtils.equals(dataSnapshot.child(mAuth!!.currentUser!!.uid).child("image").getValue(String::class.java), null)) {
                    Log.e("Select Image Update : ", " true")
                    Picasso.with(this@EditProfileActivity).load(dataSnapshot.child(mAuth!!.currentUser!!.uid).child("image").getValue(String::class.java)).resize(1000, 800)
                            .centerInside().into(selectImage)
                }

                val w_type = dataSnapshot.child(mAuth!!.currentUser!!.uid).child("worker_type").getValue(String::class.java)

                if (!TextUtils.equals(w_type, null)) {
                    Log.e("WorkerSpinner Update : ", " true")
                    if (TextUtils.equals(w_type, "Employee")) {
                        worker_type_select!!.setSelection(1)
                    } else if (TextUtils.equals(w_type, "Freelancer")) {
                        worker_type_select!!.setSelection(2)
                    }
                }

                if (dataSnapshot.child(mAuth!!.currentUser!!.uid).hasChild("work_time_limit")) {
                    val w_t_limit = dataSnapshot.child(mAuth!!.currentUser!!.uid).child("work_time_limit").getValue(Int::class.java)!!
                    Log.e("W_TimeSpinner Update : ", " true")
                    freelancer_work_time!!.setSelection(w_t_limit)
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

        save!!.setOnClickListener {
            progressDialog!!.setMessage("Profile Updating")
            progressDialog!!.show()

            if (!TextUtils.equals(worker_type_select!!.selectedItem.toString(), "")) {
                val worker_type = worker_type_select!!.selectedItem.toString()
                firebaseDatabase!!.reference.child("users").child(mAuth!!.currentUser!!.uid).child("worker_type").setValue(worker_type)
            }

            if (!TextUtils.equals(freelancer_work_time!!.selectedItem.toString(), "")) {
                val work_time_limit = freelancer_work_time!!.selectedItem.toString()
                firebaseDatabase!!.reference.child("users").child(mAuth!!.currentUser!!.uid).child("work_time_limit").setValue(Integer.parseInt(work_time_limit))
            }

            if (imageUri != null) {
                val filepath = firebaseStorage!!.reference.child("User_Images").child(imageUri!!.lastPathSegment)

                filepath.putFile(imageUri!!).addOnSuccessListener { taskSnapshot ->
                    val downloadUrl = taskSnapshot.downloadUrl
                    if (is_profile_image) {
                        firebaseDatabase!!.reference.child("users").child(mAuth!!.currentUser!!.uid).child("image").setValue(downloadUrl!!.toString())
                    } else if (is_background_image) {
                        firebaseDatabase!!.reference.child("users").child(mAuth!!.currentUser!!.uid).child("background_image").setValue(downloadUrl!!.toString())
                    }

                    progressDialog!!.dismiss()
                    if (is_admin) {
                        val toEdit = Intent(this@EditProfileActivity, AdminAccountActivity::class.java)
                        //toEdit.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        toEdit.addCategory(Intent.CATEGORY_HOME)
                        toEdit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(toEdit)
                    } else {
                        val toUser = Intent(this@EditProfileActivity, UserAccountActivity::class.java)
                        //toUser.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        toUser.addCategory(Intent.CATEGORY_HOME)
                        toUser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(toUser)
                    }
                }
            } else {
                progressDialog!!.dismiss()
                if (is_admin) {
                    val toAdmin = Intent(this@EditProfileActivity, AdminAccountActivity::class.java)
                    toAdmin.addCategory(Intent.CATEGORY_HOME)
                    toAdmin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(toAdmin)
                } else {
                    val toUser = Intent(this@EditProfileActivity, UserAccountActivity::class.java)
                    toUser.addCategory(Intent.CATEGORY_HOME)
                    toUser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(toUser)
                }
            }
        }

        authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser == null) {
                bar!!.dismiss()
                val toMain = Intent(this@EditProfileActivity, MainActivity::class.java)
                toMain.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                toMain.addCategory(Intent.CATEGORY_HOME)
                toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toMain)
            }
        }

        selectImage!!.setOnClickListener {
            is_profile_image = true
            is_background_image = false
            val galeryIntent = Intent(Intent.ACTION_GET_CONTENT)
            galeryIntent.type = "image/*"
            startActivityForResult(galeryIntent, GALLERY_REQUEST)
        }

        select_background!!.setOnClickListener {
            is_background_image = true
            is_profile_image = false
            val galeryIntent = Intent(Intent.ACTION_GET_CONTENT)
            galeryIntent.type = "image/*"
            startActivityForResult(galeryIntent, GALLERY_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY_REQUEST && resultCode == Activity.RESULT_OK ) {
            if(data != null) {
                imageUri = data.data
                Picasso.with(this@EditProfileActivity).load(imageUri).resize(1000, 800)
                        .centerInside().into(selectImage)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        mAuth!!.addAuthStateListener(authStateListener!!)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (!is_admin) {
            if (id == R.id.nav_logout) {
                val ref = FirebaseDatabase.getInstance().reference.child("users").child(mAuth!!.currentUser!!.uid).child("online")
                // ref.setValue(false);
                bar!!.setMessage("Logging out")
                bar!!.show()
                //StaticDataSingleton.getInstance().getDatabaseReference().child("users").child(StaticDataSingleton.getInstance().getUserId()).child("currentDayNumber").setValue(dayNumber_ts);
                super@EditProfileActivity.finish()
                finish()
                mAuth!!.signOut()
            } else if (id == R.id.nav_edit_profile) {
                val toEdit = Intent(this@EditProfileActivity, EditProfileActivity::class.java)
                //toEdit.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                toEdit.addCategory(Intent.CATEGORY_HOME)
                toEdit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toEdit)
            }
        } else {
            if (id == R.id.nav_logout) {
                val ref = FirebaseDatabase.getInstance().reference.child("users").child(mAuth!!.currentUser!!.uid).child("online")
                ref.setValue(false)
                bar!!.setMessage("Logging out")
                bar!!.show()
                if (StaticDataSingleton.getUser() != null) {

                    //StaticDataSingleton.getInstance().getDatabaseReference().child("users").child(StaticDataSingleton.getInstance().getUserId()).child("currentDayNumber").setValue(dayNumber_ts);
                }
                super@EditProfileActivity.finish()
                finish()
                mAuth!!.signOut()
            } else if (id == R.id.nav_edit_profile) {
                val toEdit = Intent(this@EditProfileActivity, EditProfileActivity::class.java)
                //toEdit.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                toEdit.addCategory(Intent.CATEGORY_HOME)
                toEdit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toEdit)
            } else if (id == R.id.nav_create_user) {
                val toCreate = Intent(this@EditProfileActivity, CreateUserActivity::class.java)
                //toCreate.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                toCreate.addCategory(Intent.CATEGORY_HOME)
                toCreate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toCreate)
            } else if (id == R.id.nav_list_users) {
                val toInterface = Intent(this@EditProfileActivity, UserListActivity::class.java)
                //toInterface.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                toInterface.addCategory(Intent.CATEGORY_HOME)
                toInterface.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toInterface)
            }


        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
}