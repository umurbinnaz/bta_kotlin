package com.example.umuryavuz.binnazteknikappkotlin.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.DisplayedLogInfo
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.LogInfo
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.ViewHolders.ViewHolderDisplayedLogInfo
import java.util.*

class AdapterDisplayedLogInfo : RecyclerView.Adapter<ViewHolderDisplayedLogInfo> {


    var online_logs: ArrayList<LogInfo>
    lateinit var logs: ArrayList<DisplayedLogInfo>

    constructor (arrayList: ArrayList<LogInfo>) {
        online_logs = arrayList
    }

    override fun getItemCount(): Int {

        return online_logs.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderDisplayedLogInfo {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_temp_log, parent, false)
        return ViewHolderDisplayedLogInfo(view)
    }

    override fun onBindViewHolder(holder: ViewHolderDisplayedLogInfo, position: Int) {
        var Object : LogInfo = online_logs[position]

        if (Object.logout != null) {
            val temp_date = Date(Object.logout!!)

            val temp_cal = Calendar.getInstance()
            temp_cal.time = temp_date
            temp_cal.set(Calendar.HOUR, 0)
            temp_cal.set(Calendar.MINUTE, 0)
            temp_cal.set(Calendar.SECOND, 0)
            temp_cal.set(Calendar.MILLISECOND, 0)
            val tss = temp_cal.time.time

            holder.setDate(tss)
        } else {
            holder.setDate(null)
        }

        holder.setFirstLogin(Object.login)
        holder.setLastLogout(Object.logout)
        holder.setWorkTime(Object.elapsed)
        holder.setLogIndex(position)
        if (position == getItemCount()) {
            online_logs.clear()
        }
    }


}