package com.example.umuryavuz.binnazteknikappkotlin.Mail

import android.app.Activity
import android.app.ProgressDialog
import android.os.AsyncTask
import android.util.Log

class SendMailTask : AsyncTask<Any?,Any?,Any?> {

    lateinit var sendMailActivity: Activity

    constructor(activity: Activity){
        sendMailActivity = activity

    }

     override fun onPreExecute() {
    }

     override fun doInBackground(vararg args: Any?): Any? {
        try {
            Log.i("SendMailTask", "About to instantiate GMail...")
            // publishProgress("Processing input....");
            val androidEmail = GMail(args[0].toString(),
                    args[1].toString(), args[2] as List<*>, args[3].toString(),
                    args[4].toString())
            //publishProgress("Preparing mail message....");
            androidEmail.createEmailMessage()
            //publishProgress("Sending email....");
            androidEmail.sendEmail()
            //publishProgress("Email Sent.");
            Log.i("SendMailTask", "Mail Sent.")
        } catch (e: Exception) {
            publishProgress(e.message)
            Log.e("SendMailTask", e.message, e)
        }

        return null
    }

    override fun onProgressUpdate(vararg values: Any?) {


    }

    override fun onPostExecute(result: Any?) {

    }

}