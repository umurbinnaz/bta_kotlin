package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.umuryavuz.binnazteknikappkotlin.Adapter.AdapterUserProfileViewPager
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton.mAuth
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user_profile.*
import kotlinx.android.synthetic.main.app_bar_user_profile.*
import kotlinx.android.synthetic.main.content_user_profile.*

class UserProfileActivity : AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener{

    lateinit var imageView: ImageView
    lateinit var fullname: TextView
    lateinit var page_header: ImageView
    lateinit  var mSectionsPagerAdapter: AdapterUserProfileViewPager

    companion object {
        lateinit var uid: String
        lateinit var classBundle: Bundle
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.elevation = 0f

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.setDrawerListener(toggle)
        toggle.syncState()
        toggle.isDrawerIndicatorEnabled = false

        nav_view.setNavigationItemSelectedListener(this)
        toolbar.setNavigationOnClickListener { finish() }

        fullname = findViewById(R.id.user_profile_name_surname) as TextView
        page_header = findViewById(R.id.user_profile_header_background_image) as ImageView
        mSectionsPagerAdapter = AdapterUserProfileViewPager(supportFragmentManager,tabs_user_profile.tabCount)

        container_user_profile.adapter = mSectionsPagerAdapter
        container_user_profile.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs_user_profile))
        tabs_user_profile.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container_user_profile))




        classBundle = intent.extras


        val bundle : Bundle = classBundle
        val usermail = bundle.getString("UserMail")!!.toString()
        val image = bundle.getString("Image")!!.toString()
        val LastLogin = bundle.getString("LastLogin")!!.toString()
        val LastLogout = bundle.getString("LastLogout")!!.toString()
        uid = bundle.getString("uid")!!.toString()

        val isActive = bundle.getBoolean("isActive")



        imageView = findViewById(R.id.user_profile_page_image) as ImageView

        Picasso.with(this@UserProfileActivity).load(image).resize(1000, 800)
                .centerCrop().into(imageView)



        StaticDataSingleton.DatabaseReference().child("users").child(uid).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var name: String? = ""
                var surname: String? = ""
                if (dataSnapshot.child("name").getValue(String::class.java) != null || dataSnapshot.child("secondname").getValue(String::class.java) != null) {
                    name = dataSnapshot.child("name").getValue(String::class.java)
                    surname = dataSnapshot.child("secondname").getValue(String::class.java)
                }
                val fname = "$name $surname"
                fullname.setText(fname)

                val background_image_url = dataSnapshot.child("background_image").getValue(String::class.java)

                if (background_image_url != null) {
                    Glide.with(applicationContext).load(background_image_url).into(page_header)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })


    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_logout) {

            StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).child("online").setValue(false);
            if(StaticDataSingleton.getUser() != null) {
                //    StaticDataSingleton.getInstance().getDatabaseReference().child("users").child(StaticDataSingleton.getInstance().getUserId()).child("currentDayNumber").setValue(dayNumber_ts);
            }
            finish();
            mAuth.signOut();

        } else if (id == R.id.nav_edit_profile) {
            val toEdit = Intent(this@UserProfileActivity, EditProfileActivity::class.java)
            //toEdit.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toEdit.addCategory(Intent.CATEGORY_HOME)
            toEdit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toEdit)
        } else if (id == R.id.nav_create_user) {
            val toCreate = Intent(this@UserProfileActivity, CreateUserActivity::class.java)
            //toCreate.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toCreate.addCategory(Intent.CATEGORY_HOME)
            toCreate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toCreate)
        } else if (id == R.id.nav_list_users) {
            val toInterface = Intent(this@UserProfileActivity, UserListActivity::class.java)
            //toInterface.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toInterface.addCategory(Intent.CATEGORY_HOME)
            toInterface.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toInterface)
        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
}