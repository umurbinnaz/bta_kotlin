package com.example.umuryavuz.binnazteknikappkotlin.Fragments


import android.app.DatePickerDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import java.util.*

class FragmentDateRangeLogs : Fragment() {


    lateinit var start_date: TextView
    lateinit var end_date: TextView
    lateinit var total_time: TextView
    lateinit var recyclerView: RecyclerView

    var dialog_start: DatePickerDialog? = null
    var dialog_end: DatePickerDialog? = null

     var start_y: Int = 0
     var start_m: Int = 0
     var start_d: Int = 0
     var end_y: Int = 0
     var end_m: Int = 0
     var end_d: Int = 0

    lateinit var start_date_pick: Calendar
    lateinit var end_date_pick: Calendar

    var mDateSetListener_start: DatePickerDialog.OnDateSetListener? = null
    var mDateSetListener_end: DatePickerDialog.OnDateSetListener? = null



    lateinit var rootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        rootView = inflater.inflate(R.layout.fragment_user_profile_selected_logs, container, false)

        total_time = rootView!!.findViewById(R.id.total_time) as TextView
        recyclerView = rootView!!.findViewById(R.id.user_profile_logs) as RecyclerView
        recyclerView!!.setHasFixedSize(false)
        val linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView!!.layoutManager = linearLayoutManager

        start_date = rootView!!.findViewById(R.id.start_date) as TextView
        end_date = rootView!!.findViewById(R.id.end_date) as TextView

        val cal = Calendar.getInstance()
        val year = cal.get(Calendar.YEAR)
        val month = cal.get(Calendar.MONTH)
        val day = cal.get(Calendar.DAY_OF_MONTH)

        dialog_start = DatePickerDialog(
                rootView!!.context,
                R.style.DialogTheme,
                mDateSetListener_start,
                year, month, day)

        dialog_end = DatePickerDialog(
                rootView!!.context,
                R.style.DialogTheme,
                mDateSetListener_end,
                year, month, day)

        start_date!!.text = (month + 1).toString() + "/" + (day - 7) + "/" + year
        end_date!!.text = (month + 1).toString() + "/" + day + "/" + year

        end_date_pick = Calendar.getInstance()
        end_date_pick.set(Calendar.YEAR, year)
        end_date_pick.set(Calendar.MONTH, month)
        end_date_pick.add(Calendar.MONTH, 1)
        end_date_pick.set(Calendar.DAY_OF_MONTH, day)
        end_date_pick.set(Calendar.HOUR, 0)
        end_date_pick.set(Calendar.MINUTE, 0)
        end_date_pick.set(Calendar.SECOND, 0)
        end_date_pick.set(Calendar.MILLISECOND, 0)

        start_date_pick = Calendar.getInstance()
        start_date_pick.set(Calendar.YEAR, year)
        start_date_pick.set(Calendar.MONTH, month)
        start_date_pick.set(Calendar.DAY_OF_MONTH, day)
        start_date_pick.add(Calendar.DAY_OF_MONTH, -7)
        start_date_pick.set(Calendar.HOUR, 0)
        start_date_pick.set(Calendar.MINUTE, 0)
        start_date_pick.set(Calendar.SECOND, 0)
        start_date_pick.set(Calendar.MILLISECOND, 0)


        StaticDataSingleton.getValuesFromDate(start_date_pick.timeInMillis, end_date_pick.timeInMillis, total_time, recyclerView)


        start_y = year
        start_m = month
        start_d = day - 7
        end_y = year
        end_m = month
        end_d = day

        start_date!!.setOnClickListener {
            dialog_start = DatePickerDialog(
                    rootView!!.context,
                    R.style.Base_ThemeOverlay_AppCompat_Dialog,
                    mDateSetListener_start,
                    start_y, start_m, start_d)

            dialog_start!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog_start!!.show()
        }

        end_date!!.setOnClickListener {
            dialog_end = DatePickerDialog(
                    rootView!!.context,
                    R.style.Base_ThemeOverlay_AppCompat_Dialog,
                    mDateSetListener_end,
                    end_y, end_m, end_d)

            dialog_end!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog_end!!.show()
        }

        mDateSetListener_start = DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
            var month = month
            month = month
            start_y = year
            start_m = month
            start_d = day
            val date = (month + 1).toString() + "/" + day + "/" + year
            start_date!!.text = date

            start_date_pick.set(Calendar.YEAR, start_y)
            start_date_pick.set(Calendar.MONTH, start_m)
            start_date_pick.set(Calendar.DAY_OF_MONTH, start_d)
            start_date_pick.set(Calendar.HOUR, 0)
            start_date_pick.set(Calendar.MINUTE, 0)
            start_date_pick.set(Calendar.SECOND, 0)
            start_date_pick.set(Calendar.MILLISECOND, 0)

            StaticDataSingleton.getValuesFromDate(start_date_pick.timeInMillis, end_date_pick.timeInMillis, total_time, recyclerView)
        }

        mDateSetListener_end = DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
            var month = month
            month = month
            end_y = year
            end_m = month
            end_d = day
            val date = (month + 1).toString() + "/" + day + "/" + year
            end_date!!.text = date

            end_date_pick.set(Calendar.YEAR, end_y)
            end_date_pick.set(Calendar.MONTH, end_m)
            end_date_pick.set(Calendar.DAY_OF_MONTH, end_d)
            end_date_pick.set(Calendar.HOUR, 0)
            end_date_pick.set(Calendar.MINUTE, 0)
            end_date_pick.set(Calendar.SECOND, 0)
            end_date_pick.set(Calendar.MILLISECOND, 0)

            StaticDataSingleton.getValuesFromDate(start_date_pick.timeInMillis, end_date_pick.timeInMillis, total_time, recyclerView)
        }





        return rootView
    }
}
