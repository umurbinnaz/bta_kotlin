package com.example.umuryavuz.binnazteknikappkotlin.ViewHolders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.R

class ViewHolderTaskInfo(var mView: View) : RecyclerView.ViewHolder(mView) {

    fun setTitle(tit: String) {
        val title = mView.findViewById<View>(R.id.task_title) as TextView
        title.text = tit
    }

    fun setDescription(desc: String) {
        val description = mView.findViewById<View>(R.id.description) as TextView
        description.text = desc
    }


}
