package com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects

class SlackRequest {
    private var error: String? = null
    private var response: String? = null

    constructor(error: String, response: String) {
        this.error = error
        this.response = response
    }

    constructor(){}

    fun getError(): String? {
        return error
    }

    fun setError(error: String) {
        this.error = error
    }

    fun getResponse(): String? {
        return response
    }

    fun setResponse(response: String) {
        this.response = response
    }
}