package com.example.umuryavuz.binnazteknikappkotlin.Fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.umuryavuz.binnazteknikappkotlin.Activities.CreateTaskActivity
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.TaskInfo
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.Activities.UserProfileActivity
import com.example.umuryavuz.binnazteknikappkotlin.Activities.UserProfileActivity.Companion.uid
import com.example.umuryavuz.binnazteknikappkotlin.ViewHolders.ViewHolderTaskInfo
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class FragmentTasks : Fragment() {

    lateinit var rootView: View
    lateinit var recyclerView_tasks: RecyclerView
    lateinit var task_recyclerAdapter: FirebaseRecyclerAdapter<TaskInfo, ViewHolderTaskInfo>
    var task_count: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.frament_user_profile_set_task, container, false)
        val add_task = rootView.findViewById(R.id.add_task) as Button

        recyclerView_tasks = rootView.findViewById(R.id.user_profile_tasks_list) as RecyclerView
        recyclerView_tasks.setHasFixedSize(true)
        recyclerView_tasks.setLayoutManager(LinearLayoutManager(activity))

        val query_task = StaticDataSingleton.DatabaseReference().child("users").child(UserProfileActivity.uid).child("Tasks")
        val options = FirebaseRecyclerOptions.Builder<TaskInfo>()
                .setQuery(query_task, TaskInfo::class.java!!)
                .build()

        task_recyclerAdapter = object : FirebaseRecyclerAdapter<TaskInfo, ViewHolderTaskInfo>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderTaskInfo {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.row_task, parent, false)

                return ViewHolderTaskInfo(view)
            }

            override fun onBindViewHolder(viewHolder: ViewHolderTaskInfo, position: Int, model: TaskInfo) {
                viewHolder.setTitle(model.title)
                viewHolder.setDescription(model.description)
            }


        }

        recyclerView_tasks.setAdapter(task_recyclerAdapter)
        task_recyclerAdapter.startListening()

        add_task.setOnClickListener {
            StaticDataSingleton.DatabaseReference().child("users").child(uid).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    task_count = dataSnapshot.child("task_count").getValue(Int::class.java)!!
                    StaticDataSingleton.DatabaseReference().child("users").child(uid).child("Tasks").child("task_$task_count").child("is_done").setValue(false)
                    StaticDataSingleton.DatabaseReference().child("users").child(uid).child("Tasks").child("task_$task_count").child("title").setValue("This is title $task_count")
                    StaticDataSingleton.DatabaseReference().child("users").child(uid).child("Tasks").child("task_$task_count").child("description").setValue("This is description $task_count")

                    StaticDataSingleton.DatabaseReference().child("users").child(uid).child("task_count").setValue(task_count)

                    val task_builder = Intent(activity!!.application, CreateTaskActivity::class.java)
                    task_builder.putExtra("selected_uid", uid)
                    task_builder.putExtra("task_count", task_count.toString() + "")
                    task_count++
                    startActivity(task_builder)

                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })
        }

        return rootView
    }


}