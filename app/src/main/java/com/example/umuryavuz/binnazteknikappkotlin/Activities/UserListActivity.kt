package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import com.example.umuryavuz.binnazteknikappkotlin.Adapter.AdapterOrderByDepartment
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.UserInfo
import com.example.umuryavuz.binnazteknikappkotlin.Interfaces.InterfaceItemClickListener
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.example.umuryavuz.binnazteknikappkotlin.ViewHolders.ViewHolderUserList
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.ArrayList

class UserListActivity : AppCompatActivity() , NavigationView.OnNavigationItemSelectedListener {
    lateinit var recyclerView: RecyclerView
    lateinit var context: Context
    lateinit var mAuth: FirebaseAuth
    lateinit var authStateListener: FirebaseAuth.AuthStateListener
    lateinit var lastlogin: String
    lateinit var lastlogout: String
    lateinit var user_list: ArrayList<UserInfo>
    lateinit var order_by_select: Spinner
    lateinit var order_by_dept: Spinner
    lateinit var dept_names: String
    lateinit var users: ArrayList<UserInfo>
    lateinit var options: FirebaseRecyclerOptions<UserInfo>
    lateinit var fireabaseRecylerAdapter: FirebaseRecyclerAdapter<UserInfo, ViewHolderUserList>

    @SuppressLint("ResourceType", "NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_interface)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.elevation = 0f

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.isDrawerIndicatorEnabled = false
        toggle.syncState()

        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        toolbar.setNavigationOnClickListener { finish() }

        context = applicationContext

        recyclerView = findViewById<View>(R.id.user_list) as RecyclerView
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.layoutManager = LinearLayoutManager(this)

        options = FirebaseRecyclerOptions.Builder<UserInfo>()
                .setQuery(StaticDataSingleton.DatabaseReference(), UserInfo::class.java!!)
                .build()

        fireabaseRecylerAdapter = object : FirebaseRecyclerAdapter<UserInfo, ViewHolderUserList>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderUserList {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.row_user_info, parent, false)
                return ViewHolderUserList(view)
            }

             override fun onBindViewHolder(viewHolder: ViewHolderUserList, position: Int, model: UserInfo) {
                viewHolder.setMail(model.name + " " + model.secondname)
                viewHolder.setOnline(model.online)
                viewHolder.setImage(applicationContext, model.image)
                viewHolder.setLastLogin(model.lastlogin)
                viewHolder.setDate(model.lastlogin)
                viewHolder.setItemClickListenerInterface(object : InterfaceItemClickListener {
                   override fun onClick(view: View, position: Int, isLongClick: Boolean) {

                        val goToProfile = Intent(this@UserListActivity, UserProfileActivity::class.java)
                        goToProfile.putExtra("UserMail", model.mail)
                        goToProfile.putExtra("Image", model.image)

                        if (model.lastlogin != null) {
                            goToProfile.putExtra("LastLogin", model.lastlogin.toString() + "")
                        } else {
                            goToProfile.putExtra("LastLogin", "Never login")
                        }
                        if (model.lastlogout != null) {
                            goToProfile.putExtra("LastLogout", model.lastlogout.toString() + "")
                        } else {
                            goToProfile.putExtra("LastLogout", "Never logout")
                        }
                        goToProfile.putExtra("isActive", model.isActive)
                        goToProfile.putExtra("uid", model.uid)

                       print(model.uid)

                        goToProfile.addCategory(Intent.CATEGORY_HOME)
                        goToProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(goToProfile)
                    }
                })
            }
        }

        recyclerView!!.adapter = fireabaseRecylerAdapter
        fireabaseRecylerAdapter!!.startListening()

        user_list = ArrayList<UserInfo>()
        mAuth = FirebaseAuth.getInstance()
        order_by_select = findViewById<View>(R.id.order_by_select) as Spinner
        order_by_dept = findViewById<View>(R.id.order_by_dept) as Spinner
        order_by_select!!.setSelection(0)

        authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser == null) {
                val toMain = Intent(this@UserListActivity, MainActivity::class.java)
                //toMain.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                toMain.addCategory(Intent.CATEGORY_HOME)
                toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toMain)
            }
        }

        order_by_dept!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                recyclerView!!.removeAllViews()
                recyclerView!!.destroyDrawingCache()
                users = ArrayList<UserInfo>()
                StaticDataSingleton.DatabaseReference().child("users").addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        for (ds in dataSnapshot.children) {
                            if (TextUtils.equals(order_by_dept!!.selectedItem.toString(), "All")) {
                                if (TextUtils.equals(order_by_select!!.selectedItem.toString(), "All")) {
                                    users.add(ds.getValue(UserInfo::class.java)!!)
                                } else if (order_by_select!!.selectedItem == "") {
                                    users.add(ds.getValue(UserInfo::class.java)!!)
                                } else if (TextUtils.equals(order_by_select!!.selectedItem.toString(), "Online")) {
                                    if (ds.child("online").getValue(Boolean::class.java)!!) {
                                        users.add(ds.getValue(UserInfo::class.java)!!)
                                    }
                                } else {
                                    if ((!ds.child("online").getValue(Boolean::class.java)!!)!!) {
                                        users.add(ds.getValue(UserInfo::class.java)!!)
                                    }
                                }
                            } else {
                                if (ds.hasChild("department")) {
                                    dept_names = (ds.child("department").child("name").value as String?)!!
                                    if (dept_names.contains(order_by_dept!!.selectedItem.toString())) {
                                        if (TextUtils.equals(order_by_select!!.selectedItem.toString(), "All")) {
                                            users.add(ds.getValue(UserInfo::class.java)!!)
                                        } else if (order_by_select!!.selectedItem == "") {
                                            users.add(ds.getValue(UserInfo::class.java)!!)
                                        } else if (TextUtils.equals(order_by_select!!.selectedItem.toString(), "Online")) {
                                            if (ds.child("online").getValue(Boolean::class.java)!!) {
                                                users.add(ds.getValue(UserInfo::class.java)!!)
                                            }
                                        } else {
                                            if ((!ds.child("online").getValue(Boolean::class.java)!!)!!) {
                                                users.add(ds.getValue(UserInfo::class.java)!!)
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        val mAdapter = AdapterOrderByDepartment(users, this@UserListActivity)
                        recyclerView!!.adapter = mAdapter


                    }

                    override fun onCancelled(databaseError: DatabaseError) {

                    }
                })

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        order_by_select!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                if (order_by_select!!.selectedItem != "") {
                    recyclerView!!.removeAllViews()
                    recyclerView!!.destroyDrawingCache()
                    users = ArrayList<UserInfo>()
                    StaticDataSingleton.DatabaseReference().child("users").addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            for (ds in dataSnapshot.children) {

                                if (TextUtils.equals(order_by_select!!.selectedItem.toString(), "All")) {
                                    if (!TextUtils.equals(order_by_dept!!.selectedItem.toString(), "All")) {
                                        if (ds.hasChild("department")) {
                                            dept_names = (ds.child("department").child("name").value as String?)!!
                                            if (dept_names.contains(order_by_dept!!.selectedItem.toString())) {
                                                users.add(ds.getValue(UserInfo::class.java)!!)
                                            }
                                        }
                                    } else {
                                        users.add(ds.getValue(UserInfo::class.java)!!)
                                    }
                                } else if (TextUtils.equals(order_by_select!!.selectedItem.toString(), "Online")) {
                                    if (ds.child("online").getValue(Boolean::class.java)!!) {
                                        if (!TextUtils.equals(order_by_dept!!.selectedItem.toString(), "All")) {
                                            if (ds.hasChild("department")) {
                                                dept_names = (ds.child("department").child("name").value as String?)!!
                                                if (dept_names.contains(order_by_dept!!.selectedItem.toString())) {
                                                    users.add(ds.getValue(UserInfo::class.java)!!)
                                                }
                                            }
                                        } else {
                                            users.add(ds.getValue(UserInfo::class.java)!!)
                                        }
                                    }
                                } else {
                                    if ((!ds.child("online").getValue(Boolean::class.java)!!)!!) {
                                        if (!TextUtils.equals(order_by_dept!!.selectedItem.toString(), "All")) {
                                            if (ds.hasChild("department")) {
                                                dept_names = (ds.child("department").child("name").value as String?)!!
                                                if (dept_names.contains(order_by_dept!!.selectedItem.toString())) {
                                                    users.add(ds.getValue(UserInfo::class.java)!!)
                                                }
                                            }
                                        } else {
                                            users.add(ds.getValue(UserInfo::class.java)!!)
                                        }
                                    }
                                }
                            }

                            val mAdapter = AdapterOrderByDepartment(users, this@UserListActivity)
                            recyclerView!!.adapter = mAdapter


                        }

                        override fun onCancelled(databaseError: DatabaseError) {

                        }
                    })
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }


    }

    override fun onPause() {
        super.onPause()
        fireabaseRecylerAdapter!!.stopListening()
    }

    override fun onStart() {
        super.onStart()

        mAuth!!.addAuthStateListener(authStateListener!!)


    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_logout) {
            val ref = FirebaseDatabase.getInstance().reference.child("users").child(mAuth!!.currentUser!!.uid).child("online")
            ref.setValue(false)
            if (StaticDataSingleton.getUser() != null) {
                //    StaticDataSingleton.getInstance().getDatabaseReference().child("users").child(StaticDataSingleton.getInstance().getUserId()).child("currentDayNumber").setValue(dayNumber_ts);
            }
            super@UserListActivity.finish()
            finish()
            mAuth!!.signOut()
        } else if (id == R.id.nav_edit_profile) {
            val toEdit = Intent(this@UserListActivity, EditProfileActivity::class.java)
            //toEdit.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toEdit.addCategory(Intent.CATEGORY_HOME)
            toEdit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toEdit)
        } else if (id == R.id.nav_create_user) {
            val toCreate = Intent(this@UserListActivity, CreateUserActivity::class.java)
            //toCreate.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toCreate.addCategory(Intent.CATEGORY_HOME)
            toCreate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toCreate)
        } else if (id == R.id.nav_list_users) {
            val toInterface = Intent(this@UserListActivity, UserListActivity::class.java)
            //toInterface.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toInterface.addCategory(Intent.CATEGORY_HOME)
            toInterface.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toInterface)
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
}