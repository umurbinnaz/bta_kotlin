package com.example.umuryavuz.binnazteknikappkotlin.ViewHolders

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.umuryavuz.binnazteknikappkotlin.Interfaces.InterfaceItemClickListener
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.squareup.picasso.Picasso
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ViewHolderUserList(var mView: View) : RecyclerView.ViewHolder(mView), View.OnClickListener, View.OnLongClickListener {

    private var itemClickListenerInterface: InterfaceItemClickListener? = null

    private val timestamp: Long = 0


    override fun onLongClick(v: View): Boolean {
        itemClickListenerInterface!!.onClick(v, adapterPosition, true)
        return false
    }

    override fun onClick(v: View) {
        itemClickListenerInterface!!.onClick(v, adapterPosition, false)
    }


    init {

        mView.setOnClickListener(this)
        mView.setOnClickListener(this)
    }

    fun setItemClickListenerInterface(itemClickListenerInterface: InterfaceItemClickListener) {
        this.itemClickListenerInterface = itemClickListenerInterface
    }


    fun setMail(fname: String) {
        val fullname = mView.findViewById(R.id.user_email) as TextView
        fullname.text = fname
    }

    fun setLastLogin(lastLogin: Long?) {
        val Last_Login = mView.findViewById(R.id.lastLogin) as TextView
        if (lastLogin != null) {
            try {
                val c = Calendar.getInstance()
                c.time = Date(lastLogin)
                Last_Login.text = to24Hours(lastLogin)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }
    }

    fun setDate(lastlogin: Long?) {
        val login_date = mView.findViewById(R.id.login_date) as TextView
        if (lastlogin != null) {
            val c = Calendar.getInstance()
            c.time = Date(lastlogin)
            c.add(Calendar.MONTH, 1)
            login_date.text = c.get(Calendar.YEAR).toString() + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.DAY_OF_MONTH)
        }
    }


    fun setImage(ctx: Context, image: String?) {
        if (image != null) {
            val profileImage = mView.findViewById(R.id.list_profile_image) as ImageView
            Picasso.with(ctx).load(image).resize(1000, 800)
                    .centerInside().into(profileImage)
        } else {
            val profileImage = mView.findViewById(R.id.list_profile_image) as ImageView
            Picasso.with(ctx).load("https://firebasestorage.googleapis.com/v0/b/test1-dd82b.appspot.com/o/User_Images%2FNo-Image.jpg?alt=media&token=faec0662-40fb-410d-89ba-d2856f98958a").resize(1000, 800)
                    .centerInside().into(profileImage)
        }
    }

    fun setOnline(online: Boolean) {
        if (online) {
            //  TextView online_Status = (TextView) mView.findViewById(R.id.online_status);
            val online_Status_circle = mView.findViewById(R.id.online_status_circle) as TextView
            val circleShape = online_Status_circle.background as GradientDrawable
            circleShape.setColor(Color.GREEN)
            //   online_Status.setText("Online");
        } else {
            //  TextView online_Status = (TextView) mView.findViewById(R.id.online_status);
            val online_Status_circle = mView.findViewById(R.id.online_status_circle) as TextView
            val circleShape = online_Status_circle.background as GradientDrawable
            circleShape.setColor(Color.RED)
            //  online_Status.setText("Offline");
        }
    }

    @Throws(ParseException::class)
    private fun to24Hours(time: Long?): String {
        val d = Date(time!!)
        val now = SimpleDateFormat("hh:mm aa").format(d.time)

        val inFormat = SimpleDateFormat("hh:mm aa")
        val outFormat = SimpleDateFormat("HH:mm")

        return outFormat.format(inFormat.parse(now))
    }
}