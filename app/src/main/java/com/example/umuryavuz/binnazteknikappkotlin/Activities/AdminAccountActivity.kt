package com.example.umuryavuz.binnazteknikappkotlin.Activities

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.NotificationCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.example.umuryavuz.binnazteknikappkotlin.Adapter.AdapterAdminAccountViewPager
import com.example.umuryavuz.binnazteknikappkotlin.DatabaseObjects.StaticDataSingleton
import com.example.umuryavuz.binnazteknikappkotlin.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import java.util.*

class AdminAccountActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var last_login_time: Long? = null
    var last_logout_time: Long? = null
    var isUser = true

    lateinit var mAuth: FirebaseAuth
    lateinit var authStateListener: FirebaseAuth.AuthStateListener
    lateinit var bar: ProgressDialog
    lateinit var firebaseDatabase: FirebaseDatabase
    lateinit var databaseReference: DatabaseReference
    lateinit var imageUri: Uri
    lateinit var firebaseStorage: FirebaseStorage
    lateinit var progressDialog: ProgressDialog
    lateinit var mViewPager: ViewPager
    lateinit var adapterAdminAccountViewPager: AdapterAdminAccountViewPager
    lateinit var online: TextView
    lateinit var fullname: TextView
    lateinit var page_header: ImageView
    lateinit var profileImage: ImageView
    lateinit var ts: Date
    lateinit var total_work_time: String
    lateinit var builder: NotificationCompat.Builder
    lateinit var notificationManager: NotificationManager
    lateinit var notification: Notification
    lateinit var chronometer: Chronometer
    lateinit var mContentView: RemoteViews
    lateinit var v1: ValueEventListener
    lateinit var v2: ValueEventListener
    lateinit var v3: ValueEventListener
    lateinit var v4: ValueEventListener
    lateinit var v5: ValueEventListener
    lateinit var v6: ValueEventListener
    lateinit var v7: ValueEventListener
    lateinit var v8: ValueEventListener
    lateinit var v9: ValueEventListener
    lateinit var dss: Map<String, String>
    lateinit var wmbPreference: SharedPreferences


    companion object {
        lateinit var onlineButton: Button
        var is_online: Boolean = false
        var login_count: Int = 0
        var timeElapsed: Chronometer? = null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_account)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.elevation = 0f

        StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).child("CurrentTimestamp").setValue(ServerValue.TIMESTAMP)

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
        val tabLayout = findViewById<View>(R.id.tabs) as TabLayout

        adapterAdminAccountViewPager = AdapterAdminAccountViewPager(supportFragmentManager, tabLayout.tabCount)
        mViewPager = findViewById<View>(R.id.container) as ViewPager
        mViewPager!!.adapter = adapterAdminAccountViewPager
        mViewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(mViewPager))

        firebaseStorage = FirebaseStorage.getInstance()
        firebaseDatabase = FirebaseDatabase.getInstance()

        databaseReference = firebaseDatabase!!.reference
        profileImage = findViewById<View>(R.id.profile_image) as ImageView
        online = findViewById<View>(R.id.admin_online) as TextView
        onlineButton = findViewById<View>(R.id.online) as Button
        fullname = findViewById<View>(R.id.content_admin_name_surname) as TextView
        page_header = findViewById<View>(R.id.content_admin_header_background_image) as ImageView
        progressDialog = ProgressDialog(this)
        mAuth = FirebaseAuth.getInstance()
        bar = ProgressDialog(this)

        timeElapsed = findViewById<View>(R.id.chronomete) as Chronometer
        timeElapsed!!.onChronometerTickListener = Chronometer.OnChronometerTickListener { c ->
            val cTextSize = c.text.length
            if (cTextSize == 5) {
                timeElapsed!!.text = "00:" + c.text.toString()
            } else if (cTextSize == 7) {
                timeElapsed!!.text = "0" + c.text.toString()
            }
        }
        onlineButton.setOnClickListener { StaticDataSingleton.performRecord(this@AdminAccountActivity, onlineButton, timeElapsed!!, login_count, is_online) }

        authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser == null) {
                bar!!.dismiss()
                isUser = false
                val toMain = Intent(this@AdminAccountActivity, MainActivity::class.java)
                toMain.addCategory(Intent.CATEGORY_HOME)
                toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toMain)
            } else {
                isUser = true
            }
        }

        StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.child("online").getValue(Boolean::class.javaPrimitiveType)!!) {
                    if (dataSnapshot.hasChild("chronometer_val")) {
                        if (dataSnapshot.child("chronometer_val").getValue(Int::class.java) != 0) {
                            timeElapsed!!.base = SystemClock.elapsedRealtime() - dataSnapshot.child("chronometer_val").getValue(Long::class.java)!!
                            timeElapsed!!.start()
                        }
                    }
                } else {

                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })

        if (isUser) {
            v6 = StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).child("image").addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val profile_image_url = dataSnapshot.getValue(String::class.java)
                    if (profile_image_url != null) {
                        Picasso.with(this@AdminAccountActivity).load(profile_image_url).resize(1000, 800)
                                .centerCrop().into(profileImage)
                    }
                }
                override fun onCancelled(databaseError: DatabaseError) {}
            })
            v7 = StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).addValueEventListener(object : ValueEventListener {
                @SuppressLint("ResourceAsColor")
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    if (dataSnapshot.hasChild("daily_login_counter")) {
                        login_count = dataSnapshot.child("daily_login_counter").getValue(Int::class.java)!!
                    } else {
                        StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).child("daily_login_counter").setValue(0)
                    }

                    if (dataSnapshot.child("online").getValue(Boolean::class.javaPrimitiveType)!!) {
                        online!!.setText(R.string.online)
                        is_online = true


                        onlineButton.setBackgroundResource(R.drawable.button_red)
                        onlineButton.setText(R.string.stop_record)
                        onlineButton.setTextColor(R.color.White)

                    } else {
                        online!!.setText(R.string.offline)
                        is_online = false
                        onlineButton.setBackgroundResource(R.drawable.button_green)
                        onlineButton.setText(R.string.start_record)
                        onlineButton.setTextColor(R.color.Black)
                    }
                    var name: String? = ""
                    var surname: String? = ""
                    if (dataSnapshot.child("name").getValue(String::class.java) != null || dataSnapshot.child("secondname").getValue(String::class.java) != null) {
                        name = dataSnapshot.child("name").getValue(String::class.java)
                        surname = dataSnapshot.child("secondname").getValue(String::class.java)
                    }
                    val fname = "$name $surname"
                    fullname!!.text = fname

                    last_login_time = dataSnapshot.child("lastlogin").getValue(Long::class.java)
                    last_logout_time = dataSnapshot.child("lastlogout").getValue(Long::class.java)

                    val background_image_url = dataSnapshot.child("background_image").getValue(String::class.java)

                    if (background_image_url != null) {
                        Glide.with(applicationContext).load(background_image_url).into(page_header!!)
                    }


                }
                override fun onCancelled(databaseError: DatabaseError) {}
            })
        }
    }

    override fun onDestroy() {
        if (is_online) {
            StaticDataSingleton.DatabaseReference().child("users").child(StaticDataSingleton.getUserId()).child("chronometer_val").setValue(SystemClock.elapsedRealtime() - timeElapsed!!.base)
        }
        super.onDestroy()

    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            finish()

            super.onBackPressed()
        }
    }

    override fun onStart() {
        super.onStart()
        mAuth!!.addAuthStateListener(authStateListener!!)
        authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser == null) {
                bar!!.dismiss()
                isUser = false
                val toMain = Intent(this@AdminAccountActivity, MainActivity::class.java)
                toMain.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                toMain.addCategory(Intent.CATEGORY_HOME)
                toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(toMain)
                finish()
            } else {
                isUser = true
            }
        }


    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_logout) {
            val ref = FirebaseDatabase.getInstance().reference.child("users").child(mAuth!!.currentUser!!.uid).child("online")
            ref.setValue(false)
            bar!!.setMessage("Logging out")
            bar!!.show()
            mAuth!!.removeAuthStateListener(authStateListener!!)
            super@AdminAccountActivity.finish()
            finish()
            mAuth!!.signOut()
        } else if (id == R.id.nav_edit_profile) {
            val toEdit = Intent(this@AdminAccountActivity, EditProfileActivity::class.java)
            //toEdit.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toEdit.addCategory(Intent.CATEGORY_HOME)
            toEdit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toEdit)
        } else if (id == R.id.nav_create_user) {
            val toCreate = Intent(this@AdminAccountActivity, CreateUserActivity::class.java)
            //toCreate.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toCreate.addCategory(Intent.CATEGORY_HOME)
            toCreate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toCreate)
        } else if (id == R.id.nav_list_users) {
            val toInterface = Intent(this@AdminAccountActivity, UserListActivity::class.java)
            //toInterface.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            toInterface.addCategory(Intent.CATEGORY_HOME)
            toInterface.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(toInterface)
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }


}